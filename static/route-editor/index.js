function setPermalink(map, point, zoom) {
    point = (point) ? point : [37.6155600, 55.7522200];
    zoom = (zoom) ? zoom : 11;
    let state = {
        "mapCenter": point,
        "mapZoom": zoom
    };
    let title = "Creating map state";
    let url = "#" +
        point[0] + "," +
        point[1] + "," +
        zoom;
    map.setCenter(point);
    map.setZoom(zoom);
    window.history.pushState(state, title, url);
    map.on("moveend", function () {
        let lng = Math.round(map.getCenter().lng * 100000) / 100000;
        let lat = Math.round(map.getCenter().lat * 100000) / 100000;
        point = [lng, lat];
        zoom = Math.round(map.getZoom());
        state = {
            "mapCenter": point,
            "mapZoom": zoom
        };
        url = "#" +
            point[0] + "," +
            point[1] + "," +
            zoom;
        title = "Changing map state";
        window.history.pushState(state, title, url);
    });
}
let check = document.getElementById("match");
mapboxgl.accessToken = "pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw";
// mapboxgl.accessToken = "pk.eyJ1Ijoidm9yY2h1bjMyNSIsImEiOiJjam5icDczZjM1aWRkM3dvOGl6d2pyaDdkIn0.QDmcFSHpGRhfL1ZKB1p0Lw";
let map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/light-v9",
    // center: [37.6155600, 55.7522200],
    center: [55.7522200, 37.6155600],
    bearing: 0,
    zoom: 13
});
let language = new MapboxLanguage({defaultLanguage: "ru"});
map.addControl(language);
function loadData(draw) {
    let requestBody = JSON.stringify({
        "name": "СМ-1"
    });
    let urlRoutes = "https://niikeeper.com/mvp_test/v0.4.2/ngpt/route";
    let urlStops = "https://niikeeper.com/mvp_test/v0.4.2/ngpt/route_stops";
    (async () => {
        try {
            let response = await fetch(urlRoutes, {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: requestBody,
            });
            let data = await response.json();
            data.features.forEach(function(d) {
                draw.add(d);
            });

            // Закоментировать, если не нужно зумироваться на маршрут
            let coordinates = data.features[0].geometry.coordinates;
            var bounds = coordinates.reduce(function(bounds, coord) {
                return bounds.extend(coord);
            }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
            map.fitBounds(bounds, {
                padding: 20
            });
        }
        catch (e) {
            console.log("fetch error", e);
        }
    })();
    (async () => {
        try {
            let response = await fetch(urlStops, {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: requestBody,
            });
            let data = await response.json();
            data.features.forEach(function(d) {
                // eik4: заполняем объект
                stop[d.id] = [...d.geometry.coordinates];
                draw.add(d);
            });
        }
        catch (e) {
            console.log("fetch error", e);
        }
    })();
}
// eik4: объект с остановками, ключ - id, значение - lnglat-координаты
var stop = {};
// eik4: информация о кликнутой остановке
var clickedStop = {
    id: null, // eik4: id кликнутой остановки, null - если ни одна остановка не кликнута
    circle: null // eik4: 50м вокруг остановки, null - если ни одна остановка не кликнута
};
var modes = MapboxDraw.modes;
console.log(MapboxDraw)
modes.direct_select.onDrag = function(state, e) {
    if (state.selectedCoordPaths.length === 0) {
        return;
    }
    if (state.canDragMove !== true) return;
    state.dragMoving = true;
    e.originalEvent.stopPropagation();
    
    const delta = {
        lng: e.lngLat.lng - state.dragMoveLocation.lng,
        lat: e.lngLat.lat - state.dragMoveLocation.lat
    };
    if (state.selectedCoordPaths.length > 0) this.dragVertex(state, e, delta);
    else this.dragFeature(state, e, delta);
    
    state.dragMoveLocation = e.lngLat;
};

modes.simple_select.onDrag = function(state, e) {
    let feature = this.getSelected();
    if (feature[0].type === 'LineString'){
        return
    };
    if (state.canDragMove) return this.dragMove(state, e);
    if (this.drawConfig.boxSelect && state.canBoxSelect) return this.whileBoxSelect(state, e);
};

map.on("load", function() {
    let hash = window.location.hash.split("#")[1];
    let c = [];
    let z = 10;
    if (hash && hash.split(",")) {
        c = [hash.split(",")[0], hash.split(",")[1]];
        z = hash.split(",")[2];
    } else {
        c = [this.getCenter().lng, this.getCenter().lat];
        z = this.getZoom();
    }
    setPermalink(this, c, z);
    map.loadImage("img/bus_gray.png", function(error, image) {
        if (error) throw error;
        map.addImage("bus_stop", image);
    });
    map.loadImage("img/bus_drag.png", function(error, image) {
        if (error) throw error;
        map.addImage("bus_stop_drag", image);
    });
    // Добавляем данные и стили для Draw()
    let draw = new MapboxDraw({
        modes: modes,
        displayControlsDefault: false,
        userProperties: true,
        controls: {
            point: true,
            // line_string: true,
            undo: true,
            trash: true
        },
        styles: [
            // Средняя точка полигона
            {
                "id": "gl-draw-polygon-midpoint",
                "type": "circle",
                "filter": ["all", ["==", "$type", "Point"],
                    ["==", "meta", "midpoint"]
                ],
                "paint": {
                    "circle-radius": 7,
                    "circle-color": "#ff6d6d"
                }
            },
            // Линия деактивирована
            {
                "id": "gl-draw-line-inactive",
                "type": "line",
                "filter": ["all", ["==", "active", "false"],
                    ["==", "$type", "LineString"],
                    ["!=", "mode", "static"]
                ],
                "layout": {
                    "line-cap": "round",
                    "line-join": "round"
                },
                "paint": {
                    "line-color": ["get", "user_color"],
                    "line-width": 3
                }
            },
            // Линия активирована
            {
                "id": "gl-draw-line-active",
                "type": "line",
                "filter": ["all", ["==", "$type", "LineString"],
                    ["==", "active", "true"]
                ],
                "layout": {
                    "line-join": "round"
                },
                "paint": {
                    "line-color": "#ff6d6d",
                    "line-dasharray": [3, 1],
                    "line-width": 4
                }
            },
            // Контур вокруг деактивированной точки при рисовании полигона или линии
            {
                "id": "gl-draw-polygon-and-line-vertex-stroke-inactive",
                "type": "circle",
                "filter": ["all", ["==", "meta", "vertex"],
                    ["==", "$type", "Point"],
                    ["!=", "mode", "static"]
                ],
                "paint": {
                    "circle-radius": 10,
                    "circle-color": "#910b0b"
                }
            },
            // Точки полигона и линии деактивированы
            {
                "id": "gl-draw-polygon-and-line-vertex-inactive",
                "type": "circle",
                "filter": ["all", ["==", "meta", "vertex"],
                    ["==", "$type", "Point"],
                    ["!=", "mode", "static"]
                ],
                "paint": {
                    "circle-radius": 8,
                    "circle-color": "#ff6d6d"
                }
            },
            // Контур деактивированной точки
            {
                "id": "gl-draw-point-point-stroke-inactive",
                "type": "circle",
                "filter": ["all", ["==", "active", "false"],
                    ["==", "$type", "Point"],
                    ["==", "meta", "feature"],
                    ["!=", "mode", "static"]
                ],
                "paint": {
                    "circle-radius": 7,
                    "circle-color": "#fff"
                }
            },
            // Точка деактивирована
            {
                "id": "gl-draw-point-inactive",
                "type": "symbol",
                "filter": ["all", ["==", "active", "false"],
                    ["==", "$type", "Point"],
                    ["==", "meta", "feature"],
                    ["!=", "mode", "static"]
                ],
                "layout": {
                    "icon-allow-overlap": true,
                    "icon-image": "bus_stop",
                    "icon-size": {
                        stops: [[11, 0.05], [13, 0.08], [16, 0.45]]
                    }
                }
            },
            // Контур активированной точки
            {
                "id": "gl-draw-point-stroke-active",
                "type": "circle",
                "filter": ["all", ["==", "$type", "Point"],
                    ["==", "active", "true"],
                    ["==", "meta", "midpoint"],
                    ["==", "meta", "feature"],
                ],
                "paint": {
                    "circle-radius": 7,
                    "circle-color": "#ff6d6d"
                }
            },
            // Точка активирована
            {
                "id": "gl-draw-point-active",
                "type": "symbol",
                "filter": ["all",
                    ["==", "$type", "Point"],
                    ["==", "meta", "feature"],
                    ["==", "active", "true"],
                ],
                "layout": {
                    "icon-allow-overlap": true,
                    "icon-image": "bus_stop_drag",
                    "icon-size": {
                        stops: [[11, 0.05], [13, 0.08], [16, 0.45]]
                    }
                }
            },
            
            // Статичная линия
            {
                "id": "gl-draw-line-static",
                "type": "line",
                "filter": ["all", ["==", "mode", "static"],
                    ["==", "$type", "LineString"]
                ],
                "layout": {
                    "line-cap": "round",
                    "line-join": "round"
                },
                "paint": {
                    "line-color": "#f0f0f0",
                    "line-width": 5
                }
            },
            // Статичная точка
            {
                "id": "gl-draw-point-static",
                "type": "symbol",
                "filter": ["all", ["==", "mode", "static"],
                    ["==", "$type", "Point"]
                ],
                "layout": {
                    "icon-allow-overlap": true,
                    "icon-image": "bus_stop",
                    "icon-size": {
                        stops: [[11, 0.05], [13, 0.08], [16, 0.45]]
                    }
                }
            }
        ]
    });
    
    map.addControl(draw, "bottom-left");
    let ctrl = document.getElementsByClassName("mapboxgl-ctrl");
    for (i = 0; i < ctrl.length; i++) {
        let child = ctrl[i].children;
        if (child.length) {
            for (let j = 0; j < child.length; j++) {
                if (child[j].classList.contains("mapbox-gl-draw_point")) {
                    child[j].title = "Добавить остановку";
                    let btn = document.createElement("button");
                    btn.title = "Привязать к графу";
                    btn.classList.add("mapbox-gl-draw_graph");
                    
                    ctrl[i].insertBefore(btn, ctrl[i].lastElementChild);
                    btn.addEventListener("click", function() {
                        this.classList.toggle("graph_active");
                        btn.classList.contains("graph_active") ? check.checked = true : check.checked = false;
                        console.log(check.checked);
                    });
                } else if (child[j].classList.contains("mapbox-gl-draw_trash")) {
                    child[j].title = "Удалить";
                }
            }
            
        }
        
    }
    loadData(draw);
    // Привязка функций к событиям
    map.on("draw.create", updateRoute);
    map.on("draw.update", updateRoute);
    map.on("draw.delete", removeRoute);
    map.on("draw.selectionchange", selectionchange);
    function selectionchange(e) {
        if (e.features.length === 1) {
            let f = e.features[0];
            if (clickedStop.id) {
                map.removeLayer("stopArea");
                map.removeLayer("stopArea_border");
                map.removeSource("stopArea");
                map.removeSource("stopArea_border");
            }
            // eik4: если кликнутый объект является остановкой
            if (stop[f.id]) {
                // eik4: сохраняем id кликнутой остановки
                clickedStop.id = f.id;
                let center = stop[f.id];
                let radius = 0.05;
                let options = {steps: 100, units: "kilometers"};
                let circle = turf.circle(center, radius, options);
                clickedStop.circle = circle;
                // eik4: добавляем окружность вокруг остановки
                map.addLayer({
                    "id": "stopArea",
                    "type": "fill",
                    "source": {
                        "type": "geojson",
                        "data": circle
                    },
                    "paint": {
                        "fill-color": "#008000",
                        "fill-opacity": 0.1
                    }
                });
                map.addLayer({
                    "id": "stopArea_border",
                    "type": "line",
                    "source": {
                        "type": "geojson",
                        "data": circle
                    },
                    "paint": {
                        "line-color": "#ff6d6d",
                        "line-dasharray": [3, 1],
                        "line-width": 3
                    }
                })
            }
            else {
                clickedStop.id = null;
                clickedStop.circle = null;
            }
        }
        else {
            if (clickedStop.id) {
                map.removeLayer("stopArea");
                map.removeLayer("stopArea_border");
                map.removeSource("stopArea");
                map.removeSource("stopArea_border");
            }
            clickedStop.id = null;
            clickedStop.circle = null;
        }
    }
    // Use the coordinates you just drew to make the Map Matching API request
    function updateRoute(e) {
            // eik4: проверка на то, что была сдвинута остановка
            let stopMovedFlag = false;
            if (e.features.length === 1) {
                if (stop[e.features[0].id]) {
                    stopMovedFlag = true;
                }
            }
            if (stopMovedFlag) { // eik4: если остановка была сдвинута
                let newFeature = e.features[0];
                let isInsideCircle = turf.booleanPointInPolygon(newFeature, clickedStop.circle);
                if (!isInsideCircle) { // eik4: если остановка была сдвинута за нарисованный круг
                    // alert("out of bounds");
                    draw.delete(clickedStop.id); // eik4: удаляем остановку
                    newFeature.geometry.coordinates = [...stop[clickedStop.id]]; // eik4: у ее фичи меняем центр
                    draw.add(newFeature); // eik4: рисуем ее
                    // eik4: обнуляем выбранную остановку и стираем круг
                    clickedStop.id = null;
                    clickedStop.circle = null;
                    map.removeLayer("stopArea");
                    map.removeLayer("stopArea_border");
                    map.removeSource("stopArea");
                    map.removeSource("stopArea_border");
                }
            } else { // eik4: если остановка не была сдвинута, то выполняется старый код
                removeRoute(); // Перезаписываем слой
                var profile = "driving"; // Устанавливаем профиль (автомобильный)
                // Вытаскиваем выделенное
                var selectedObject = draw.getSelected();
                var coords = [];
                if (selectedObject.features.length === 0) { // Новая геометрия
                    var data = draw.getAll();
                    var lastFeature = data.features.length - 1;
                    coords = data.features[lastFeature].geometry.coordinates;
                } else { // Существующая геометрия
                    coords = selectedObject.features[0].geometry.coordinates
                }
                // Форматирование координат
                var newCoords = coords.join(";");
                // Установка радиусов в 25 метров
                var radius = [];
                coords.forEach(element => {
                    radius.push(25);
                });
                if (check.checked) {
                    if (selectedObject.features.length === 0) { // Новая геометрия
                        data.features[lastFeature].properties.color = '#da70d6';
                        getMatch(data.features[lastFeature], newCoords, radius, profile);
                        draw.delete(data.features[lastFeature].id);
                    } else { // Существующая геометрия
                        getMatch(selectedObject.features[0], newCoords, radius, profile);
                        draw.delete(selectedObject.features[0].id);
                    }
                }
            }
    }
    // Запрос в MapMatching API
    function getMatch(feature, coordinates,radius,profile) {
        let query = `https://api.mapbox.com/matching/v5/mapbox/${profile}?access_token=${mapboxgl.accessToken}`;
        (async () => {
            try {
                let response = await fetch(query, {
                    headers: {
                        "Accept": "application/json",
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    method: "POST",
                    body: `coordinates=${coordinates}`
                });
                let data = await response.json();
                let coordsEncoded = data.matchings[0].geometry;
                // Парсинг закодированной геометрии
                let coordsDecoded = polyline.decode(coordsEncoded);
                coordsDecoded.forEach(function(elem){
                    elem.reverse();
                });
                coordsDecoded = {
                    coordinates : coordsDecoded,
                    type : "LineString"
                };
                let matchedRoute = {
                    "type": "Feature",
                    "id": feature.id,
                    "properties": feature.properties,
                    "geometry": coordsDecoded
                };
                addRoute(matchedRoute);
            }
            catch (e) {
                // coordsDecoded = {
                //     coordinates : coordinates,
                //     type : "LineString"
                // };
                // addRoute(coordsDecoded);
                console.log("fetch error", e);
            }
        })();
    }
    // Отрисовка маршрута
    function addRoute(matchedRoute) {
        // Если маршрут уже загружен, то лучше его удалить
        if (map.getSource("route")) {
            map.removeLayer("route");
            map.removeSource("route");
        } else {
            // Добавим объект в Draw
            draw.add(matchedRoute);
        };
    }
    function removeRoute() {
        if (map.getSource("route")) {
            map.removeLayer("route");
            map.removeSource("route");
        }
    }
    
});
var popup = new mapboxgl.Popup({
    closeButton: false,
    closeOnClick: false
});
map.on('mouseenter', 'gl-draw-point-inactive.cold', function(e) {
    map.getCanvas().style.cursor = 'pointer';
    var coordinates = e.features[0].geometry.coordinates.slice();
    var direction = (e.features[0].properties.user_dir === 0)? 'прямое' : 'обратное';
    var description = e.features[0].properties.user_name + ' (' + direction + ' направление)';
    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    popup.setLngLat(coordinates)
        .setHTML(description)
        .addTo(map);
});

map.on('mouseleave', 'gl-draw-point-inactive.cold', function() {
    map.getCanvas().style.cursor = '';
    popup.remove();
});