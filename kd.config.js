const pkg = require('./package')
const webpack = require('webpack')
const ru = {
  "dataIterator": {
    "rowsPerPageText": "Items per page:",
    "rowsPerPageAll": "Все",
    "pageText": "{0}-{1} из {2}",
    "noResultsText": "Не найдено соответствующих записей",
    "nextPage": "Следующая страница",
    "prevPage": "Предыдущая страница"
  },
  "dataTable": {
    "rowsPerPageText": "Строк на странице:"
  },
  "noDataText": "Нет доступных данных"
};

module.exports = {
  env: {
    kd: true
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'АИС Маршрутная сеть',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.50.0/mapbox-gl.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300&display=swap&subset=cyrillic'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Open+Sans:300,400&display=swap&subset=cyrillic'
      },
      {
        rel: 'stylesheet',
        href: 'https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.css'
      },
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  css: ['~assets/styles/app.scss'],
  /*
  ** Build configuration
  */
  plugins: [
    '~/plugins/axios',
    '~/plugins/filters.js',
    {
      src: './plugins/snotify',
      ssr: false
    },
    '~/plugins/EventBus',
    { src: '~/plugins/Mapbox', ssr: false },
    '~/plugins/VTooltip',
    '~/plugins/VueBar',
    '~/plugins/Draggable',
    '~/plugins/Vuetify',
    '~/plugins/downloadExcel',
  ],
  modules: [
    '@nuxtjs/axios',
    ['@nuxtjs/vuetify', {
      lang: {
        locales: { ru },
        current: 'ru'
      },
    }]
  ],
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
        config.module.noParse = /(mapbox-gl)\.js$/
      }
    },
    plugins: [
      new webpack.ProvidePlugin({
        mapboxgl: 'mapbox-gl',
        MapboxLanguage: '@mapbox/mapbox-gl-language',
        MapboxDraw: '@mapbox/mapbox-gl-draw'
      })
    ],
    vendor: ['mapbox-gl', '@mapbox/mapbox-gl-language', 'mapbox-gl-vue', '@mapbox/mapbox-gl-draw', 'vuetify'],
    transpile: ['vue-json-excel'],
  }
}

