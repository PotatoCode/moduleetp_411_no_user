import connection from '~/assets/js/connection';

const defaultState = {
  myStats: {
    feedbackIssues: 0,
    feedbackProposals: 0,
    pollsCommentaries: 0,
    pollsVotes: 0,
    proposals: 0,
    proposalsCommentaries: 0,
    proposalsLikes: 0,
    proposalsVotes: 0,
    proposalsWIP: 0,
    proposalsWIP_Closed: 0
  }
};

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_ACHIEVEMENT(state, myStats) {
    state.myStats = myStats;
  },
  CLEAR_ACHIEVEMENT_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
};

export const actions = {
  getAchievement({ commit }) {
    return this.$axios.get(
      `${connection.getAddress()}/account/profile/settings/common/view/mystats`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      commit('SET_ACHIEVEMENT', data);
    });
  },
  clearAchievementStore({ commit }) {
    commit('CLEAR_ACHIEVEMENT_STORE')
  }
};
