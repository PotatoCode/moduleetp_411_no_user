import connection from '~/assets/js/connection';

// Округа
const defaultState = {
  divisions: [],
}

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_DIVISIONS(state, data) {
    state.divisions = data
  },
  CLEAR_DIVISIONS_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
};

export const actions = {
  async getDivisions({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/divisions`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    commit('SET_DIVISIONS', data.features)
  },
  clearDivisionsStore({ commit }) {
    commit('CLEAR_DIVISIONS_STORE')
  }
}
