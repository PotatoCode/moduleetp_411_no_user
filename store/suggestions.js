// Предложения

export const state = () => ({
  suggestions: [
    {
      name: 'Автобус №288',
      district: 'Сосенское',
      description: 'В часы пик (утром и вечером) загруженность автобуса 288, следующего из Бутово в Коммунарку, неимоверная.',
      text: 'Например, вечером на остановке "метро Бунинская аллея»…',
      status: {
        text: 'На рассмотрении',
        value: 'underConsideration'
      },
      userId: 1,
    },
    {
      name: 'Автобус №514',
      district: 'Михайлово-Ярцевское',
      description: 'Жители деревни Ярцево и многочисленные дачники СНТ "Ярцево" просят организовать заезд автобуса 514 в деревню.',
      text: 'В деревне постоянно проживают жители. Постороен…',
      status: {
        text: 'Реализовано',
        value: 'realised'
      },
      userId: 2,
    },
    {
      name: 'Автобус №642',
      district: 'Ясенево',
      description: 'Запустить новый маршрут автобуса от р-на Ясенево Юго-Западного округа г.Москвы до станции метро Саларьево.',
      text: 'Для того чтобы добраться до Метро Саларьево приходится…',
      status: {
        text: 'Отклонено',
        value: 'refused'
      },
      userId: 3,
    }
  ],
  isVotedIds: [],
});

export const mutations = {
  SET_SUGGESTIONS(state, data) {
    state.suggestions = data;
  },
  ADD_SUGGESTION(state, data) {
    state.suggestions.push(data);
  },
  ADD_VOTED_SUGGESTION_ID(state, data) {
    state.isVotedIds.push(data)
  }
};

