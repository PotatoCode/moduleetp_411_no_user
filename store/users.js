import connection from '~/assets/js/connection';

// Пользователи

export const state = () => ({
  homePages: {
    user: '/',
    operator: '/operator/proposals',
    admin: '/admin/users',
    CM: '/content-manager/polls',
  },
  pages: {
    user: ['index', 'login', 'register', 'restore-password', 'editor', 'proposals', 'votes', 'vote-id', 'account', 'account-proposals', 'account-notifications', 'account-messages', 'account-achievement', 'gateway_auth', 'votes-stats'],
    operator: ['login', 'editor', 'account', 'operator-proposals', 'operator-comments', 'votes-stats', 'content-manager-polls'],
    admin: ['login', 'account', 'admin-users', 'admin-proposals', 'admin-operators', 'votes-stats'],
    CM: ['login', 'account', 'content-manager-polls', 'votes-stats'],
  },
  role: '',
});

export const mutations = {
  SET_USER_ROLE(state, role) {
    state.role = role;
  },
};

export const actions = {
  async getRole( {commit} ) {
    await this.$axios.get(
      `${connection.getAddress()}/whoami`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then((response) => {
      commit('SET_USER_ROLE', response.data.role);
    })
    .catch((err) => {
      if (err.toString().includes('403')) {
        localStorage.removeItem("token");
        this.$router.push('/login')
      }
    })
    // commit('SET_USER_ROLE', data.role);
  },
  setAsUser({commit}) {
    commit('SET_USER_ROLE', 'user');
  }
};
