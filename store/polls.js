import connection from '~/assets/js/connection';

// Опросы
const defaultState = {
  allPolls: [],
  pollsList: [],
  pollsRemaining: 0,
  pollsRoutes: [],
  pollRoute: [],
  pollStops: [],
  discussions: [],
  discussionsSending: false,
  discussionsLoading: false,
  stopFrom: null,
  stopTo: null,
  routeParams: null,
};

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_POLL_ROUTE(state, data) {
    state.pollRoute = data
  },
  SET_POLL_STOPS(state, data) {
    state.pollStops = data
  },
  SET_POLLS_ROUTES(state, data) {
    state.pollsRoutes = data
  },
  SET_POLLS_LIST(state, data) {
    state.pollsList = data
  },
  ADD_POLLS(state, data) {
    state.pollsList = [...state.pollsList, ...data]
  },
  CLEAR_POLLS_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  },
  SET_POLL_DISCUSSIONS(state, data) {
    state.discussions = data;
  },
  SET_DISCUSSIONS_SENDING(state, data) {
    state.discussionsSending = data;
  },
  SET_DISCUSSIONS_LOADING(state, data) {
    state.discussionsLoading = data;
  },
  SET_POLLS_REMAINING(state, data) {
    state.pollsRemaining = data
  },
  SET_ALL_POLLS(state, data) {
    state.allPolls = data
  },
  SET_STOP_FROM(state, data) {
    state.stopFrom = data
  },
  SET_STOP_TO(state, data) {
    state.stopTo = data
  },
  SET_ROUTE_PARAMS(state, data) {
    state.routeParams = data
  }
};

export const actions = {
  async getAllPolls({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/polls/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_ALL_POLLS', data.data);
  },
  async getAllPolls071({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress({ version: 'v0.7.1' })}/moscow/polls/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_ALL_POLLS', data.data);
  },
  async getPollsRoutes({commit}) {
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/polls/ngpt/routes/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_POLLS_ROUTES', data.data)
  },
  async getPollsList({commit}, params) {
    let paramsStr = '';
    if (params) {
      for (let i in params) {
        if (i !== 'reset') {
          paramsStr += `${i}=${typeof params[i] === 'object' ? JSON.stringify(params[i]) : params[i]}&`;
        }
      }
    }
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/polls/list${paramsStr.length !== 0 ? `?${paramsStr}` : ''}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    if (data.remaining !== undefined) {
      commit('SET_POLLS_REMAINING', data.remaining)
    }
    if (!params || (params && params.reset)) {
      commit('SET_POLLS_LIST', data.data)
    } else {
      commit('ADD_POLLS', data.data)
    }
  },
  saveVote({actions}, vote) {
    this.$axios.post(
      `${connection.getAddress()}/moscow/polls/vote`,
      vote,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(() => {
      this.dispatch('polls/getAllPolls');
      $nuxt.$snotify.success(
        'Ваш голос успешно учтен',
        {
          position: 'centerTop',
          titleMaxLength: 100
        }
      );
    }).catch(() => {
      $nuxt.$snotify.error(
        'Ошибка',
        {
          position: 'centerTop',
          titleMaxLength: 100
        }
      );
    });
  },
  saveVote071({actions}, vote) {
    this.$axios.post(
      `${connection.getAddress({version: 'v0.7.1'})}/moscow/polls/vote`,
      vote,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(() => {
      this.dispatch('polls/getAllPolls');
      $nuxt.$snotify.success(
        'Ваш голос успешно учтен',
        {
          position: 'centerTop',
          titleMaxLength: 100
        }
      );
    }).catch(() => {
      $nuxt.$snotify.error(
        'Ошибка',
        {
          position: 'centerTop',
          titleMaxLength: 100
        }
      );
    });
  },
  async getPollStopsById({commit}, routeID) {
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/polls/ngpt/stops/byRouteID/${routeID}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_POLL_STOPS', data.features)
  },
  async getPollStopsByPollId071({commit}, pollID) {
    const {data} = await this.$axios.get(
      `${connection.getAddress({ version: 'v0.7.1' })}/moscow/polls/byGUID/${pollID}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    const stops = [];
    ['forwardStopsParams', 'backwardStopsParams'].forEach((key) => {
      if (data[key]) {
        data[key].stopsNew.forEach((stop) => {
          stops.push({
            geometry: {
              coordinates: stop.coordinates,
              type: "Point"
            },
            id: stop.id,
            properties: {
              dir: key === 'forwardStopsParams' ? 0 : 1,
              sequenceID: stop.sequenceID,
              fullname: stop.fullname,
              name: stop.name,
              id: stop.id,
              stopID: stop.stopID,

            },
            type: "Feature"
          })
        })
      }
    })
    const mainRouteStops = stops.filter((s) => {return s.properties.dir === 0}).sort((a,b) => {return a.properties.sequenceID > b.properties.sequenceID});
    commit('SET_POLL_STOPS', stops);
    commit('SET_STOP_FROM', mainRouteStops[0].properties.fullname);
    commit('SET_STOP_TO', mainRouteStops[mainRouteStops.length-1].properties.fullname);
    commit('SET_ROUTE_PARAMS', {
      forwardLength: data.forwardLength,
      backwardLength: data.backwardLength,
      summaryLength: data.summaryLength,
      peakIntervalMinutes: data.peakIntervalMinutes,
      semipeakIntervalMinutes: data.semipeakIntervalMinutes
    });
  },
  async getPollRouteById({commit}, routeID) {
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/polls/ngpt/routes/byRouteID/${routeID}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_POLL_ROUTE', data.features)
  },
  clearPollsStore({commit}) {
    commit('CLEAR_POLLS_STORE')
  },
  async getPollDiscussions({commit}, data) {
    if (data.loading) {
      commit('SET_DISCUSSIONS_LOADING', true);
    }
    this.$axios.get(
      `${connection.getAddress()}/moscow/polls/discussions/byPollGUID//${data.id}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(({data}) => {
      commit('SET_POLL_DISCUSSIONS', data.data);
      commit('SET_DISCUSSIONS_LOADING', false);
    }).catch(() => {
      console.error('getPollDiscussions error');
    });
  },
  async sendPollDiscussionsComment({commit}, data) {
    commit('SET_DISCUSSIONS_SENDING', true);
    let url = `${connection.getAddress()}/moscow/polls/discussions/post/${data.guid}/${data.parentGid}`;
    this.$axios.post(
      url,
      {
        textField: data.textField,
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(() => {
      this.dispatch('polls/getPollDiscussions', {
        id: data.guid
      });
      commit('SET_DISCUSSIONS_SENDING', false);
    }).catch(() => {
      console.error('sendPollDiscussionsComment error');
    });
  }
}
