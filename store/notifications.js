import connection from '~/assets/js/connection';

const defaultState = {
  all: {},
  count: 0,
  categories: {},
  categoriesById: {},
  subCategories: {},
  subCategoriesByCategoryId: [],
  refs: [],
};

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_ALL_NOTIFICATIONS(state, data) {
    state.all = data;
  },
  SET_NOTIFICATIONS_COUNT(state, count) {
    state.count = count;
  },
  SET_CATEGORIES(state, categories) {
    state.categories = categories;
  },
  SET_CATEGORIES_BY_ID(state, categoriesById) {
    state.categoriesById = categoriesById;
  },
  SET_SUB_CATEGORIES(state, subCategories) {
    state.subCategories = subCategories;
  },
  SET_SUB_CATEGORIES_BY_CATEGORY_ID(state, subCategoriesByCategoryId) {
    state.subCategoriesByCategoryId = subCategoriesByCategoryId;
  },
  SET_REFS(state, refs) {
    state.refs = refs;
  },
};

export const actions = {
  getAllNotifications({commit}) {
    this.$axios.get(
      `${connection.getAddress()}/account/notifications/all`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      commit('SET_ALL_NOTIFICATIONS', data.data.reduce((acc, curr) => {
        if (!acc[curr.category]) {
          acc[curr.category] = [curr];
        } else {
          acc[curr.category].push(curr);
        }
        return acc;
      }, {}));
      commit('SET_NOTIFICATIONS_COUNT', data.data.filter(n => !n.seen).length);
    });
  },
  async getNotificationsCount({commit}) {
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/account/notifications/count`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_NOTIFICATIONS_COUNT', data.data);
  },
  async setNotificationSeen({}, guid) {
    await this.$axios.get(
      `${connection.getAddress()}/account/notifications/seen/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    this.dispatch('notifications/getAllNotifications');
  },
  getCategories({commit}) {
    return this.$axios.get(
      `${connection.getAddress()}/account/profile/settings/subscriptions/categories`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      commit('SET_CATEGORIES', data.data.reduce((acc, curr) => {
        acc[curr.category] = curr;
        return acc;
      }, {}));
      commit('SET_CATEGORIES_BY_ID', data.data.reduce((acc, curr) => {
        acc[curr.id] = curr;
        return acc;
      }, {}));
    });
  },
  getSubCategories({commit}) {
    return this.$axios.get(
      `${connection.getAddress()}/account/profile/settings/subscriptions/subCategories`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      commit('SET_SUB_CATEGORIES', data.data.reduce((acc, curr) => {
        acc[curr.subCategory] = curr;
        return acc;
      }, {}));
      commit('SET_SUB_CATEGORIES_BY_CATEGORY_ID', data.data.reduce((acc, curr) => {
        if (!acc[curr.categoryID]) {
          acc[curr.categoryID] = [curr]
        } else {
          acc[curr.categoryID].push(curr);
        }
        return acc;
      }, {}));
    });
  },
  getRefs({commit}) {
    return this.$axios.get(
      `${connection.getAddress()}/account/profile/settings/subscriptions/refs`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      commit('SET_REFS', data.data);
    });
  },
  subscribe({}, params) {
    return this.$axios.get(
      `${connection.getAddress()}/account/profile/settings/subscriptions/subscribe`,
      {
        params,
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
  },
  unsubscribe({}, params) {
    return this.$axios.get(
      `${connection.getAddress()}/account/profile/settings/subscriptions/unsubscribe`,
      {
        params,
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
  }
};
