export const state = () => ({
  data: {
    masterRoute: null,
    subMenuOpen: true,
    cablewayEdit: false,
    // route2: {
    //     "type":"Feature","id":"1937","properties":{"actualDate":"2018-10-25","agencyName":"МОСГОРТРАНС","interval":"5 – 10","isMain":true,"lengthDoc":15,"lengthKm":15,"releaseTs":"18","routeDirection":0,"routeFullname":"Метро \"Юго-Западная\" - Киевский вокзал","routeID":"524","routeName":"34","stops":[5574,5547,10278,5548,5655,5656,5549,1000825,5550,5551,5552,5553,5554,5555,5556,5447,11336,5448,5557,5450,5558,5559,5560,5561,5562,5563,2411,2548,2412,2413,11205,5564],"transportType":"Тб","typeTs":"БК"},"geometry":{"type":"MultiLineString",
    //         "coordinates":
    //             [
    //                 [[37.56372561325716, 55.76333774106985], [37.56419921180523, 55.765221895074745], [37.56460515340862, 55.76710595803192]],
    //                 [[37.56460515340862, 55.76710595803192], [37.565315551230725, 55.76919925453808], [37.5655523505047, 55.76967498807096]],
    //                 [[37.5655523505047, 55.76967498807096], [37.567818857828, 55.76954178326733], [37.569950051294086, 55.7693895486461]],
    //                 [[37.569950051294086, 55.7693895486461], [37.5719459309031, 55.769256342876105], [37.57245335792339, 55.76923731343939], [37.57258867178055, 55.770341005261656], [37.57285929952684, 55.77108312530001]],
    //                 [[37.57285929952684, 55.77108312530001], [37.57296078491177, 55.77180620302727], [37.57316375571341, 55.77252926734144]],
    //                 [[37.57316375571341, 55.77252926734144], [37.57424626669865, 55.773004960226444], [37.576106832418475, 55.77283371145771]]
    //             ]
    // }},
    // linesPoints: {},
    // routeLength: 0,
    // routeEndCoordinates: null,
  }
});

export const actions = {
  async getMasterRoute({commit, state}, params) {
    if (typeof window.$nuxt.$loading.start === 'function') {
      window.$nuxt.$loading.start();
    }
    const data = await new Promise(resolve => {
      setTimeout(() => {
        resolve({
          mainRoute: {
            stops: {
              features: [{
                "geometry": {"coordinates": [37.60208546, 55.8244832], "type": "Point"},
                "id": 403,
                "properties": {"stopType": "master", "name": "Ул. Кашенкин Луг"},
                "type": "Feature"
              }, {
                "geometry": {"coordinates": [37.70970085, 55.8197743], "type": "Point"},
                "id": 3696,
                "properties": {"stopType": "master", "name": "Погонный пр."},
                "type": "Feature"
              }, {
                "geometry": {"coordinates": [37.81665559, 55.82899402], "type": "Point"},
                "id": 4099,
                "properties": {"stopType": "master", "name": "Сахалинская ул."},
                "type": "Feature"
              }, {
                "geometry": {"coordinates": [37.82755046, 55.81840872], "type": "Point"},
                "id": 3964,
                "properties": {"stopType": "master", "name": "Байкальская ул."},
                "type": "Feature"
              }, {
                "geometry": {"coordinates": [37.81981326, 55.79343936], "type": "Point"},
                "id": 3633,
                "properties": {"stopType": "master", "name": "15-я Парковая ул."},
                "type": "Feature"
              }],
              type: 'FeatureCollection'
            },
            route: {
              features: [
                {"type":"Feature","geometry":{"type":"MultiLineString","coordinates":[[[37.60208546,55.8244832],[37.70970085,55.8197743]]]}},
                {"type":"Feature","geometry":{"type":"MultiLineString","coordinates":[[[37.70970085,55.8197743],[37.81665559,55.82899402]]]}},
                {"type":"Feature","geometry":{"type":"MultiLineString","coordinates":[[[37.81665559,55.82899402],[37.82755046,55.81840872]]]}},
                {"type":"Feature","geometry":{"type":"MultiLineString","coordinates":[[[37.82755046,55.81840872],[37.81981326,55.79343936]]]}}],
              type: 'FeatureCollection',
            }
          },
          backRoute: {
            stops: {
              features: [{
                "geometry": {"coordinates": [37.81981326, 55.79343936], "type": "Point"},
                "id": 3633,
                "properties": {"stopType": "master", "name": "15-я Парковая ул."},
                "type": "Feature"
              }, {
                "geometry": {"coordinates": [37.73061598, 55.76698923], "type": "Point"},
                "id": 3858,
                "properties": {"stopType": "master", "name": "8-я ул. Соколиной Горы"},
                "type": "Feature"
              }, {
                "geometry": {"coordinates": [37.70098072, 55.78350313], "type": "Point"},
                "id": 3439,
                "properties": {"stopType": "master", "name": "Электрозаводский мост"},
                "type": "Feature"
              }, {
                "geometry": {"coordinates": [37.61545396, 55.80326794], "type": "Point"},
                "id": 2947,
                "properties": {"stopType": "master", "name": "Поликлиника"},
                "type": "Feature"
              }, {
                "geometry": {"coordinates": [37.59477233, 55.81312017], "type": "Point"},
                "id": 2942,
                "properties": {"stopType": "master", "name": "Ул. Добролюбова"},
                "type": "Feature"
              }, {
                "geometry": {"coordinates": [37.60208546, 55.8244832], "type": "Point"},
                "id": 403,
                "properties": {"stopType": "master", "name": "Ул. Кашенкин Луг"},
                "type": "Feature"
              }],
              type: 'FeatureCollection'
            },
            route: {
              features: [
                {"type":"Feature","geometry":{"type":"MultiLineString","coordinates":[[[37.81981326,55.79343936],[37.73061598,55.76698923]]]}},
                {"type":"Feature","geometry":{"type":"MultiLineString","coordinates":[[[37.73061598,55.76698923],[37.70098072,55.78350313]]]}},
                {"type":"Feature","geometry":{"type":"MultiLineString","coordinates":[[[37.70098072,55.78350313],[37.61545396,55.80326794]]]}},
                {"type":"Feature","geometry":{"type":"MultiLineString","coordinates":[[[37.61545396,55.80326794],[37.59477233,55.81312017]]]}},
                {"type":"Feature","geometry":{"type":"MultiLineString","coordinates":[[[37.59477233,55.81312017],[37.60208546,55.8244832]]]}}
              ],
              type: 'FeatureCollection'
            }
          },
        });
      }, 1000);
    });
    commit('SET_DATA', {masterRoute: data});
    if (window.$nuxt.$store.state.app.data.dataLoaded) {
      window.$nuxt.$loading.finish();
    }
  }
};


export const mutations = {
  SET_DATA(state, data) {
    state.data = {
      ...state.data,
      ...data
    }
  },

  SET_CABLEWAY_EDIT(state, data) {
    state.cablewayEdit = data
  }
};
