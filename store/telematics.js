import connection from '~/assets/js/connection';

// Телематика
const defaultState = {
  loading: false,
  telematics: [],
  available: true,
};

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_TELEMATICS(state, data) {
    state.telematics = data;
  },
  SET_LOADING(state, data) {
    state.loading = data;
  },
  DISABLE(state) {
    state.available = false;
  },
  CLEAR_TELEMATICS_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
};

export const actions = {
  async getTelematics({ commit }) {
    commit('SET_LOADING', true);
    const polyline = require('@mapbox/polyline');
    const point = require('@turf/helpers').point;
    const response = await this.$axios.get(
      `${connection.getAddress({version: 'v0.7.1'})}/moscow/vehicles/telematics`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    if (response.status >= 200 && response.status < 400) {
      if (response.data.data) {
        const decodedData = response.data.data.map(itemData => {
          const routes = Object.values(this.state.ngpt.routesByGUID).filter((r) => {return r.routeID === itemData.routeId});
          const vehicleType = routes.length === 0 ? 'А' : routes[0].vehicleType;
          const icon = vehicleType === 'А' ? 'telematics-bus-icon-new' :
            vehicleType === 'Тб' ? 'telematics-trolleybus-icon-new' :
            'telematics-tram-icon-new';
          return point(polyline.decode(itemData.geom)[0], {
            bearing: itemData.bearing,
            device_id: itemData.device_id,
            company: itemData.company.replace('@', ''),
            velocity: itemData.velocity,
            licensePlate: itemData.licensePlate,
            routeName: itemData.routeName ? itemData.routeName : 'н/д',
            routeId: itemData.routeId,
            timestamp: itemData.timestamp,
            vehicleType: vehicleType,
            icon: icon
          });
        });
        commit('SET_TELEMATICS', decodedData);
        commit('SET_LOADING', false);
      }
      else {
        commit('DISABLE');
      }
    }
    else {
      commit('DISABLE');
    }
  },
  clearTelematicsStore({ commit }) {
    commit('CLEAR_TELEMATICS_STORE')
  }
};
