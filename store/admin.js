import connection from '~/assets/js/connection';

export const state = () => ({
  loading: false,
  administrators: [],
  operators: [],
  proposalHistory: [],
});

export const mutations = {
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ADMINISTRATORS(state, administrators) {
    state.administrators = administrators;
  },
  SET_OPERATORS(state, operators) {
    state.operators = operators;
  },
  SET_PROPOSAL_HISTORY(state, proposalHistory) {
    state.proposalHistory = proposalHistory;
  }
};

export const actions = {
  proposalReset({commit}, guid) {
    commit('SET_LOADING', true);
    return this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/sec/admin/reset/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      this.dispatch('proposals/getAllProposals');
      commit('SET_LOADING', false);
      return data;
    }).catch((e) => {
      commit('SET_LOADING', false);
      return {
        error: e
      };
    });
  },
  async createAdmin({commit}, params) {
    commit('SET_LOADING', true);
    return this.$axios.post(
      `${connection.getAddress()}/section/administration/administrators/create`,
      params,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) =>  {
      commit('SET_LOADING', false);
      return data;
    }).catch((e) => {
      commit('SET_LOADING', false);
      return {
        error: e
      };
    });
  },
  async createOperator({commit}, params) {
    commit('SET_LOADING', true);
    return this.$axios.post(
      `${connection.getAddress()}/section/administration/operators/create`,
      params,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) =>  {
      commit('SET_LOADING', false);
      return data;
    }).catch((e) => {
      commit('SET_LOADING', false);
      return {
        error: e
      };
    });
  },
  getAdministrators({commit}) {
    commit('SET_LOADING', true);
    return this.$axios.get(
      `${connection.getAddress()}/section/administration/administrators/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      commit('SET_ADMINISTRATORS', data.data);
      commit('SET_LOADING', false);
      return data;
    }).catch((e) => {
      commit('SET_LOADING', false);
      return {
        error: e
      };
    });
  },
  getOperators({commit}) {
    commit('SET_LOADING', true);
    return this.$axios.get(
      `${connection.getAddress()}/section/administration/operators/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      commit('SET_OPERATORS', data.data);
      commit('SET_LOADING', false);
      return data;
    }).catch((e) => {
      commit('SET_LOADING', false);
      return {
        error: e
      };
    });
  },
  getOperatorStatistics({commit}, params) {
    commit('SET_LOADING', true);
    return fetch(
        `${connection.getAddress()}/section/administration/operators/statistics/${params.guid}?from=${params.dateStart}&to=${params.dateEnd}`,
        {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        }
    )
    .then(response => response.json())
    .then(response => {
        commit('SET_LOADING', false);
        response.operatorId = params.guid;
        return response;
    })
    .catch(e => {
        commit('SET_LOADING', false);
        return {error: e};
    });
  },
  getProposalHistory({commit}, guid) {
    commit('SET_LOADING', true);
    return this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/sec/admin/proposals/history/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      commit('SET_PROPOSAL_HISTORY', data.data);
      commit('SET_LOADING', false);
      return data;
    }).catch((e) => {
      commit('SET_LOADING', false);
      return {
        error: e
      };
    });
  },
  blockOperator({commit, dispatch}, guid) {
    commit('SET_LOADING', true);
    return this.$axios.get(
      `${connection.getAddress()}/section/administration/operators/block/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      dispatch('getOperators');
      commit('SET_LOADING', false);
      return data;
    }).catch((e) => {
      commit('SET_LOADING', false);
      return {
        error: e
      };
    });
  },
  unBlockOperator({commit, dispatch}, guid) {
    commit('SET_LOADING', true);
    return this.$axios.get(
      `${connection.getAddress()}/section/administration/operators/unblock/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      dispatch('getOperators');
      commit('SET_LOADING', false);
      return data;
    }).catch((e) => {
      commit('SET_LOADING', false);
      return {
        error: e
      };
    });
  },
  changeOperator({commit}, params) {
    commit('SET_LOADING', true);
    return this.$axios.post(
      `${connection.getAddress()}/moscow/proposals/sec/admin/changeOperator`,
      params,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) =>  {
      this.dispatch('proposals/getAllProposals');
      commit('SET_LOADING', false);
      return data;
    }).catch((e) => {
      commit('SET_LOADING', false);
      return {
        error: e
      };
    });
  },

};
