import connection from '~/assets/js/connection';

export const state = () => ({
});

export const mutations = {

};

export const actions = {
  initiate({commit}, params) {
    return this.$axios.post(
      `${connection.getAddress()}/moscow/polls/sec/cm/initiate`,
      params,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  initiate071({commit}, params) {
    return this.$axios.post(
      `${connection.getAddress({version: 'v0.7.1'})}/moscow/polls/sec/cm/initiate`,
      params,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  publish({commit}, params, guid, planedCloseDate) {
    const data = {
      planedCloseDate: `${params.planedCloseDate}:00`
    };
    return this.$axios.post(`${connection.getAddress({ version: 'v0.7.1' })}/moscow/polls/sec/cm/publish/${params.guid}`,
      data,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  unPublish({commit}, guid) {
    return this.$axios.get(
      `${connection.getAddress()}/moscow/polls/sec/cm/unpublish/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  unPublish071({commit}, guid) {
    return this.$axios.get(
      `${connection.getAddress({version: 'v0.7.1'})}/moscow/polls/sec/cm/unpublish/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  delete({commit}, guid) {
    return this.$axios.get(
      `${connection.getAddress()}/moscow/polls/sec/cm/delete/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  delete071({commit}, guid) {
    return this.$axios.get(
      `${connection.getAddress({version: 'v0.7.1'})}/moscow/polls/sec/cm/delete/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  changeClosure({commit}, params) {
    return this.$axios.post(
      `${connection.getAddress()}/moscow/polls/sec/cm/changeClosure/${params.guid}`,
      {
        newPlannedCloseDate: params.newPlannedCloseDate,
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  changeClosure071({commit}, params) {
    return this.$axios.post(
      `${connection.getAddress({version: 'v0.7.1'})}/moscow/polls/sec/cm/changeClosure/${params.guid}`,
      {
        newPlannedCloseDate: params.newPlannedCloseDate,
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  republish({commit}, params) {
    return this.$axios.post(
      `${connection.getAddress()}/moscow/polls/sec/cm/republish/${params.guid}`,
      {
        newPlannedCloseDate: params.newPlannedCloseDate,
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  republish071({commit}, params) {
    return this.$axios.post(
      `${connection.getAddress({version: 'v0.7.1'})}/moscow/polls/sec/cm/republish/${params.guid}`,
      {
        newPlannedCloseDate: params.newPlannedCloseDate,
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  close({commit}, guid) {
    return this.$axios.get(
      `${connection.getAddress()}/moscow/polls/sec/cm/close/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
  close071({commit}, guid) {
    return this.$axios.get(
      `${connection.getAddress({version: 'v0.7.1'})}/moscow/polls/sec/cm/close/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
  },
};
