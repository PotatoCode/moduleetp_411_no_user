import connection from '~/assets/js/connection';

const defaultState = {
  items: [],
};

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_ITEMS(state, items) {
    state.items = items;
  },
  CLEAR_SOCIAL_OBJECTS_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
};

export const actions = {
  async getSocialObjects({ commit }) {
    const polyline = require('@mapbox/polyline');
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/socialObjects`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    const items = data.data.map(item => ({
      coordinates: polyline.decode(item.geom)[0].reverse(),
      name: item.name,
      id: item.id,
    }));
    commit('SET_ITEMS', items);
  },
  clearSocialObjects({ commit }) {
    commit('CLEAR_SOCIAL_OBJECTS_STORE')
  }
};

