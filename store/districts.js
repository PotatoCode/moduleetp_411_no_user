import connection from '~/assets/js/connection';

// Районы
const defaultState = {
  districts: [],
}

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_DISTRICTS(state, data) {
    state.districts = data
  },
  CLEAR_DISTRICTS_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
};

export const actions = {
  async getDistricts({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/districts`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    commit('SET_DISTRICTS', data.features)
  },
  clearDistrictsStore({ commit }) {
    commit('CLEAR_DISTRICTS_STORE')
  }
}
