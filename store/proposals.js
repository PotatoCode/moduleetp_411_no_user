import connection from '~/assets/js/connection';

// Районы
const defaultState = {
  proposals: [],
  proposalsRemaining: 0,
  myProposals: {},
  infoRoute: {},
  newProposal: {
    title: '',
    reason: '',
  },
  info: {},
  allProposals: [],
  proposalFilters: {},
  proposal: {},
  discussions: [],
  discussionsSending: false,
  discussionsLoading: false,
  statuses: [],
  possibleDays: {},
  possibleTimes: {},
};

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_PROPOSALS(state, data) {
    state.proposals = data
  },
  ADD_PROPOSALS(state, data) {
    state.proposals = [...state.proposals, ...data]
  },
  SET_PROPOSALS_REMAINING(state, data) {
    state.proposalsRemaining = data
  },
  SET_MY_PROPOSALS(state, data) {
    state.myProposals = data
  },
  SET_ALL_PROPOSALS(state, data) {
    state.allProposals = data
  },
  ADD_PROPOSAL(state, data) {
    const proposalInd = state.allProposals.findIndex(p => p.id === data.id);
    if (proposalInd === -1) {
      state.allProposals.push(data);
    } else {
      state.allProposals[proposalInd] = {
        ...state.allProposals[proposalInd],
        ...data,
      }
    }
    state.allProposals = [...state.allProposals];
  },
  ADD_MY_PROPOSAL(state, data) {
    state.myProposals[data.routeID] = data;
  },
  SET_INFO_ROUTE(state, data) {
    state.infoRoute = data;
  },
  SET_INFO(state, data) {
    state.info = data;
  },
  SET_NEW_PROPOSAL(state, data) {
    state.newProposal = data;
  },
  SET_PROPOSAL_FILTERS(state, data) {
    state.proposalFilters = data;
  },
  SET_PROPOSAL(state, data) {
    state.proposal = data;
  },
  CLEAR_PROPOSAL_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  },
  SET_PROPOSAL_DISCUSSIONS(state, data) {
    state.discussions = data;
  },
  SET_DISCUSSIONS_SENDING(state, data) {
    state.discussionsSending = data;
  },
  SET_DISCUSSIONS_LOADING(state, data) {
    state.discussionsLoading = data;
  },
  SET_STATUSES(state, data) {
    state.statuses = data;
  },
  SET_POSSIBLE_DAYS(state, data) {
    state.possibleDays = data;
  },
  SET_POSSIBLE_TIMES(state, data) {
    state.possibleTimes = data;
  }
};

export const actions = {
  async getProposals({ commit }, params) {
    let paramsStr = '';
    for (let i in params) {
      if (i !== 'reset') {
        paramsStr += `${i}=${typeof params[i] === 'object' ? JSON.stringify(params[i]) : params[i]}&`
      }
    }
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/list${paramsStr.length !== 0 ? `?${paramsStr}` : ''}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )

    for (var key in data.data) {
      if (data.data[key].status === 'В работе') {
        data.data[key].status = 'На рассмотрении';
      }
    }

    if (data.remaining !== undefined) {
      commit('SET_PROPOSALS_REMAINING', data.remaining)
    }
    if (params) {
      if (params.reset) {
        commit('SET_PROPOSALS', data.data)
      } else {
        commit('ADD_PROPOSALS', data.data)
      }
    }
  },
  async getMyProposals({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    commit('SET_MY_PROPOSALS', data.data.reduce((acc, curr) => {
      if (curr.isMine) {
        acc[curr.routeID] = curr;
      }
      return acc;
    }, {}));
  },
  initiateProposal({ commit }, data) {
    return new Promise(resolve => {
      this.$axios.post(
        `${connection.getAddress()}/moscow/proposals/initiate`,
        data,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
        }
      ).then(({data}) => {
        commit('ADD_MY_PROPOSAL', data)
        resolve(data);
      }).catch((err) => {
        resolve({error: true})
      });
    })
  },
  async getInfo({commit}) {
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/analytics/summary`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    commit('SET_INFO', data);
  },
  setNewProposal({ commit }, data) {
    commit('SET_NEW_PROPOSAL', data)
  },
  async getAllProposals({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    commit('SET_ALL_PROPOSALS', data.data);
  },
  async getAllProposals071({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress({ version: 'v0.7.1' })}/moscow/proposals/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    commit('SET_ALL_PROPOSALS', data.data);
  },
  async getPossibleDays({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress({ version: 'v0.7.1' })}/moscow/proposals/possibleDays`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    const obj = {};
    data.forEach((d) => {
      obj[d.id] = d;
    })
    commit('SET_POSSIBLE_DAYS', obj);
  },
  async getPossibleTimes({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress({ version: 'v0.7.1' })}/moscow/proposals/possibleTimes`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    const obj = {};
    data.forEach((d) => {
      obj[d.id] = d;
    })
    commit('SET_POSSIBLE_TIMES', obj);
  },
  setProposalFilters({ commit }, data) {
    commit('SET_PROPOSAL_FILTERS', data)
  },
  saveVote({actions}, vote) {
    this.$axios.post(
      `${connection.getAddress()}/moscow/proposals/vote`,
      vote,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(() => {
      this.dispatch('proposals/getProposal', {
        guid: vote.proposalID,
        add: true,
      });
      $nuxt.$snotify.success(
        'Ваш голос успешно учтен',
        {
          position: 'centerTop',
          titleMaxLength: 100
        }
      );
    }).catch(() => {
      $nuxt.$snotify.error(
        'Ошибка',
        {
          position: 'centerTop',
          titleMaxLength: 100
        }
      );
    });
  },
  async getProposal({ commit }, params) {
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/byGUID/${params.guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_PROPOSAL', data ? data.data[0] : {});
    if (params.add) {
      commit('ADD_PROPOSAL', data ? data.data[0] : {});
    }
  },
  async getProposal071({ commit }, params) {
    const { data } = await this.$axios.get(
      `${connection.getAddress({ version: 'v0.7.1' })}/moscow/proposals/byGUID/${params.guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_PROPOSAL', data ? data : {});
    if (params.add) {
      commit('ADD_PROPOSAL', data ? data : {});
    }
  },
  clearProposalStore({ commit }) {
    commit('CLEAR_PROPOSAL_STORE')
  },
  async getProposalDiscussions({commit}, data) {
    // id = '8fe6ace1-1525-4baa-a5cd-d7b8c15cfd6c';
    if (data.loading) {
      commit('SET_DISCUSSIONS_LOADING', true);
    }
    this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/discussions/byProposalGUID/${data.id}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(({data}) => {
      commit('SET_PROPOSAL_DISCUSSIONS', data.data);
      commit('SET_DISCUSSIONS_LOADING', false);
    }).catch(() => {
      console.error('getProposalDiscussions error');
    });
  },
  async getProposalDiscussions071({commit}, data) {
    // id = '8fe6ace1-1525-4baa-a5cd-d7b8c15cfd6c';
    if (data.loading) {
      commit('SET_DISCUSSIONS_LOADING', true);
    }
    this.$axios.get(
      `${connection.getAddress({ version: 'v0.7.1' })}/moscow/proposals/discussions/byProposalGUID/${data.id}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(({data}) => {
      commit('SET_PROPOSAL_DISCUSSIONS', data);
      commit('SET_DISCUSSIONS_LOADING', false);
    }).catch(() => {
      console.error('getProposalDiscussions error');
    });
  },
  async sendProposalDiscussionsComment({commit}, data) {
    commit('SET_DISCUSSIONS_SENDING', true);
    let url = `${connection.getAddress()}/moscow/proposals/discussions/post/${data.guid}/${data.parentGid}`;
    this.$axios.post(
      url,
      {
        textField: data.textField,
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(() => {
      this.dispatch('proposals/getProposalDiscussions', {
        id: data.guid
      });
      commit('SET_DISCUSSIONS_SENDING', false);
    }).catch(() => {
      console.error('sendProposalDiscussionsComment error');
    });
  },
  async sendProposalDiscussionsComment071({commit}, data) {
    commit('SET_DISCUSSIONS_SENDING', true);
    let url = `${connection.getAddress({ version: 'v0.7.1' })}/moscow/proposals/discussions/post/${data.guid}`;
    if (data.parentGid) {
      url = `${url}/${data.parentGid}`;
    }
    else {
      url = `${url}/root`;
    }
    this.$axios.post(
      url,
      {
        textField: data.textField,
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    ).then(() => {
      this.dispatch('proposals/getProposalDiscussions071', {
        id: data.guid
      });
      commit('SET_DISCUSSIONS_SENDING', false);
    }).catch(() => {
      console.error('sendProposalDiscussionsComment error');
    });
  },
  getStatuses({commit}, guid) {
    return this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/history/${guid}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    ).then(({data}) => {
      commit('SET_STATUSES', data.data);
    }).catch(e =>{
      console.info(e);
    })
  }
};
