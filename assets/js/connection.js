const connection = {

  mode: 'global',

  newMode: 'newGlobal',

  address: {

      local: 'http://0.0.0.0:0',
      
      global: 'https://niikeeper.com/mvp_test/v0.4.2',
      
      newGlobal: 'https://niikeeper.com/mvp_test/v0.7.1'
    
    }, //address

    versions: {

      'v0.4.2': {

        dev: 'https://niikeeper.com/mvp_test/v0.4.2',

        prod: 'https://niikeeper.com/mvp_test/v0.4.2',
        
        test: 'https://niikeeper.com/mvp_test/v0.4.2',

      },

      'v0.7.1': {

        dev: 'https://niikeeper.com/mvp_test/v0.7.1',

        prod: 'https://niikeeper.com/mvp_test/v0.7.1',

        test: 'https://niikeeper.com/mvp_test/v0.7.1',

      },

    }, // versions

    getAddress (options = {}) {

      options = {

        version: 'v0.4.2',

        env: 'dev',

        ...options

      }

      return this.versions[options.version][options.env];
    
    }, // getAddress

}

export default connection
