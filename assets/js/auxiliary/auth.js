/* Авторизация пользователя и сохранение токена в localStorage для дальнейшего использования в Get-запросах */
import connection from '~/assets/js/connection';

export default function() {
    async function getToken(name, pass) {
        try {
            let response = await fetch(`${connection.getAddress()}/doauth`, {
                method: "POST",
                mode: "cors",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    "username": name,
                    "password": pass
                })
            });
            let data = await response.json();
            if (data.Error) {
                alert("Ошибка авторизации!");
                window.location.reload();
            } else {
                localStorage.setItem("token", JSON.stringify(data));
            }
        } catch (e) {
            throw new Error(e);
        }
    }
    if (!localStorage.getItem("token")) {
        getToken("pas", "pas");
    } else {
        let expireDate = new Date(JSON.parse(localStorage.getItem("token")).expire);
        if (expireDate < new Date()) {
            localStorage.removeItem("token");
            getToken("pas", "pas");
        }
    }
};