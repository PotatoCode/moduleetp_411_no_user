function updateSaveDataBtn(disabled) {
  const saveDataBtn = document.querySelector('.save-data-btn');
  const saveDataTextArea = document.querySelector('.save-data-textarea');
  if (saveDataTextArea.value.trim().length) {
    saveDataBtn.disabled = false;
  } else {
    saveDataBtn.disabled = disabled;
  }
}

/* Создаём элементы для панели control. На входе - атрибуты title, class, disabled */
function setControlBtn(elTitle, elClass, elDisabled) {
    let el = document.createElement("button");
    el.title = elTitle;
    el.classList.add(elClass);
    el.disabled = elDisabled;
    return el;
}

/* Функция сброса features к первоначальному состоянию. На вход подаётся экземпляр MapboxDraw, optimizeMidpoints(), updateDirectionInfo() */
function resetOnClick(draw, optimizeMidpoints, updateDirectionInfo, getTimeStr) {
    let answer = confirm("Вы хотите вернуть все маршруты к первоначальному состоянию? Ваши настройки не сохранятся!");
    if (answer) {
        let initialState = JSON.parse(localStorage.getItem("forwardSteps"))[0]; // Получаем первоначальное состояние всех features
        // Если при сбросе был выделен маршрут или остановка, выделяем тот же объект после обновления features
        let selectedObj = draw.getSelected();
        draw.set(initialState);
        // Оптимизируем количество промежуточнх точек на линии маршрута
        if (selectedObj && selectedObj.features[0]) {
            if (selectedObj.features[0].geometry.type.includes("LineString")) {
                optimizeMidpoints();
            }
        }
        // Обнуляем данные для кнопок "Отменить действие"  и "Вернуть действие"
        localStorage.setItem("forwardSteps", JSON.stringify([initialState]));
        localStorage.setItem("backSteps", JSON.stringify([]));
        window.undo.disabled = true;
        updateSaveDataBtn(true);
        window.redo.disabled = true;
        updateDirectionInfo(draw, getTimeStr); // Обновляем длины маршрутов
    }
}

/* Функция возврата features на шаг назад. На вход подаётся экземпляр MapboxDraw, optimizeMidpoints(), updateDirectionInfo() */
function undoOnClick(draw, optimizeMidpoints, updateDirectionInfo, getTimeStr) {
    let forwardSteps = JSON.parse(localStorage.getItem("forwardSteps"));
    let previousStep = forwardSteps.pop();
    if (forwardSteps.length === 1) {
      window.undo.disabled = true;
      updateSaveDataBtn(true);
    }
    if (forwardSteps[forwardSteps.length - 1]) {
        // Помещаем отменённый шаг в кнопку "Вернуть действие"
        window.redo.disabled = false;
        let backSteps = JSON.parse(localStorage.getItem("backSteps"));
        backSteps.push(previousStep);
        localStorage.setItem("backSteps", JSON.stringify(backSteps));
        // Если при отмене действия был выделен маршрут или остановка, выделяем тот же объект после обновления features
        let selectedObj = draw.getSelected();
        draw.set(forwardSteps[forwardSteps.length - 1]);
        updateDirectionInfo(draw, getTimeStr); // Обновляем длины маршрутов
        // Оптимизируем количество промежуточнх точек на линии маршрута
        if (selectedObj && selectedObj.features[0]) {
            if (selectedObj.features[0].geometry.type.includes("LineString")) {
                optimizeMidpoints();
            }
        }
        // Обновляем данные для кнопки "Отменить действие"
        localStorage.setItem("forwardSteps", JSON.stringify(forwardSteps));
        optimizeMidpoints();
    } else {
        window.undo.disabled = true;
        updateSaveDataBtn(true);
    }
}

/* Функция возврата features на шаг вперёд. На вход подаётся экземпляр MapboxDraw, optimizeMidpoints(), updateDirectionInfo() */
function redoOnClick(draw, optimizeMidpoints, updateDirectionInfo, getTimeStr) {
    let backSteps = JSON.parse(localStorage.getItem("backSteps"));
    if (backSteps.length === 1) window.redo.disabled = true;
    if (backSteps[backSteps.length - 1]) {
        // Помещаем возвращённый шаг в кнопку "Отменить действие"
        let forwardSteps = JSON.parse(localStorage.getItem("forwardSteps"));
        forwardSteps.push(backSteps[backSteps.length - 1]);
        localStorage.setItem("forwardSteps", JSON.stringify(forwardSteps));
        window.undo.disabled = false;
        updateSaveDataBtn(false);
        // Если при возврате действия был выделен маршрут или остановка, выделяем тот же объект после обновления features
        let selectedObj = draw.getSelected();
        draw.set(backSteps[backSteps.length - 1]);
        updateDirectionInfo(draw, getTimeStr); // Обновляем длины маршрутов
        // Оптимизируем количество промежуточнх точек на линии маршрута
        if (selectedObj && selectedObj.features[0]) {
            if (selectedObj.features[0].geometry.type.includes("LineString")) {
                optimizeMidpoints();
            }
        }
        // Обновляем данные для кнопки "Вернуть действие"
        backSteps.pop();
        if (!backSteps.length) window.redo.disabled = true;
        localStorage.setItem("backSteps", JSON.stringify(backSteps));
        optimizeMidpoints();
    } else {
        window.redo.disabled = true;
    }
}

/* Функция сохранения изменённого маршрута */
function saveOnClick(startData, map, draw) {
    let finishData = draw.getAll();
    let changedData = {
        type: "FeatureCollection",
        features: []
    };
    let startId = [];
    let finishId = [];
    let resultArr = [];
    startData.features.forEach(function(d) {
        startId.push(d.id);
    });
    finishData.features.forEach(function(d) {
        finishId.push(d.id);
    });
    // Сравнение первоначальных и последних features.id на предмет добавления и / или удаления объекта
    for(let i = 0; i < startId.length; i++) {
        for (let j = 0; j < finishId.length; j++) {
            if (startId[i] === finishId[j]) {
                resultArr.push(startId[i]);
                startId[i] = null;
                finishId[j] = null;
            }
        }
    }
    // Добавление параметра "delete" для удалённого объекта
    startId.forEach(function(val) {
        if (val) {
            startData.features.forEach(function(f) {
                if (f.id === val) {
                    f.properties.state = "delete";
                    changedData.features.push(f);
                }
            });
        }
    });
    // Добавление параметра "add" для добавленного объекта
    finishId.forEach(function(val) {
        if (val) {
            finishData.features.forEach(function(f) {
                if (f.id === val) {
                    console.log(f);
                    f.properties.state = "add";
                    changedData.features.push(f);
                }
            })
        }
    });
    // Добавление параметра "change" для изменённого объекта
    resultArr.forEach(function(val) {
        for (let i = 0; i < startData.features.length; i++) {
            let startFeature = startData.features[i];
            for (let j = 0; j < finishData.features.length; j++) {
                let finishFeature = finishData.features[j];
                if (startFeature.id === val && startFeature.id === finishFeature.id) {
                    if (startFeature.geometry.coordinates.toString() !== finishFeature.geometry.coordinates.toString()) {
                        finishFeature.properties.state = "change";
                        changedData.features.push(finishFeature);
                    }
                }
            }
        }
    });
    // Парсинг данных для отправки на сервер
    let serverData = {
        routes: [],
        stops: []
    };
    changedData.features.forEach(function(val) {
        if (val.geometry.type.includes("LineString")) {
            serverData.routes.push(val);
        } else if (val.geometry.type.includes("Point")) {
            serverData.stops.push(val);
        }
    });
    localStorage.setItem("changes", JSON.stringify(serverData));
    alert("Ваше мнение будет учтено!");
    // Обнуляем данные для кнопок "Отменить действие"  и "Вернуть действие"
    localStorage.setItem("forwardSteps", JSON.stringify([startData]));
    localStorage.setItem("backSteps", JSON.stringify([]));
    window.undo.disabled = true;
    updateSaveDataBtn(true);
    window.redo.disabled = true;
    // Если на карте был выделен объект, убираем выделение
    draw.changeMode("simple_select");
    map.getStyle().layers.filter(l => {
        if (l.id === "stopArea") {
            map.removeLayer("stopArea");
            map.removeSource("stopArea");
        }
        if (l.id === "stopArea_border") {
            map.removeLayer("stopArea_border");
            map.removeSource("stopArea_border");
        }
    });
}

/* Кастомизация панели control */
function setControl(position, startData, map, draw, optimizeMidpoints, updateDirectionInfo, getTimeStr) {
    let infoBlock = document.getElementById("info-block");
    let fragment = document.createDocumentFragment();
    let reset = setControlBtn("Сбросить изменения", "mvp_reset", false);
    window.undo = setControlBtn("Отменить действие", "mvp_undo", true);
    window.redo = setControlBtn("Вернуть действие", "mvp_redo", true);
    let save = setControlBtn("Сохранить изменения", "mvp_save", true);
    let info = setControlBtn("Справка", "mvp_info", false);
    let center = setControlBtn("Вернуться на маршрут", "mvp_center", false);

    reset.addEventListener("click", ()=>{resetOnClick(draw, optimizeMidpoints, updateDirectionInfo, getTimeStr)});
    window.undo.addEventListener("click", ()=>{undoOnClick(draw, optimizeMidpoints, updateDirectionInfo, getTimeStr)});
    window.redo.addEventListener("click", ()=>{redoOnClick(draw, optimizeMidpoints, updateDirectionInfo, getTimeStr)});
    save.addEventListener("click", () => {saveOnClick(startData, map, draw)});
    info.addEventListener("click", function() {
        this.classList.toggle("active");
        info.classList.contains("active") ? infoBlock.style.display = "block" : infoBlock.style.display = "none";
    });
    center.addEventListener("click", function() {
        let data = draw.getAll();
        console.log(data);
        let coordinates = [];
        for (let i = 0; i < data.features.length; i++) {
            let feature = data.features[i];
            if (feature.geometry.type.includes("LineString")) {
                console.log(feature);
                feature.geometry.coordinates.forEach((c) => coordinates.push(c));
            }
        }
        let bounds = coordinates.reduce(function (bounds, coord) {
            return bounds.extend(coord);
        }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
        map.fitBounds(bounds, {
            padding: 20
        });
    });
    fragment.appendChild(info);
    fragment.appendChild(center);
    fragment.appendChild(window.undo);
    fragment.appendChild(window.redo);
    // fragment.appendChild(save);
    fragment.appendChild(reset);
    let control = document.querySelector(`.mapboxgl-ctrl-${position}`).firstElementChild;
    control.insertBefore(fragment, control.firstElementChild);
    for (let i = 0; i < control.children.length; i++) {
        let child = control.children[i];
        if (child.classList.contains("mapbox-gl-draw_point")) {
            child.title = "Добавить остановку";
        }
        if (child.classList.contains("mapbox-gl-draw_trash")) {
            child.title = "Удалить";
        }
        child.style.height = control.offsetHeight / control.children.length + "px";
    }
}

/* Загрузка действия в масив произведенных действий при изменении объекта */
function getSteps(draw) {
    let steps = JSON.parse(localStorage.getItem("forwardSteps"));
    steps.push(draw.getAll());
    localStorage.setItem("forwardSteps", JSON.stringify(steps));
    window.undo.disabled = false;
    updateSaveDataBtn(false);

}

export {setControl, getSteps, updateSaveDataBtn};
