import Vue from 'vue'
import 'mapbox-gl/dist/mapbox-gl.css'
import Mapbox from 'mapbox-gl-vue'

Vue.component('mapbox', Mapbox)