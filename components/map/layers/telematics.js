import {mapActions} from 'vuex';
import telematicsIcon from '~/assets/img/telematics.svg';
import telematicsIconNew from '~/assets/img/location.svg';
import telematicsBusIconNew from '~/assets/img/bus_location.svg';
import telematicsTrolleybusIconNew from '~/assets/img/trolleybus_location.svg';
import telematicsTramIconNew from '~/assets/img/tram_location.svg';
import telematicsBusIcon from '~/assets/img/telematics-bus.svg';
import telematicsClusterIcon from '~/assets/img/telematics-cluster.svg';
import telematicsClusterNewIcon from '~/assets/img/telematics-cluster-new.svg';

export const telematics = {
  data() {
    return {
      telematicsIconSizes: {
        width: 34,
        height: 34,
      },
    }
  },
  methods: {
    ...mapActions({
      getTelematics: 'telematics/getTelematics',
      clearTelematicsStore: 'telematics/clearTelematicsStore'
    }),
    async addTelematics(params = {}) {
      if (params.layerName === undefined) {
        await this.getTelematics();
        if (!this.$nuxt.$store.state.telematics.available) {
          this.$snotify.error(
            'Данные о телематике недоступны',
            {
              position: 'centerTop',
              titleMaxLength: 100
            }
          );
          this.editorData.currentTS.status = 'error';
        }
      }
      const defaults = {
        layerName: 'source-telematics',
        features: [...this.telematics],
        minzoom: 0,
        clusterMaxZoom: 15,
      }
      const options = {...defaults, ...params};
      if (!this.map.getSource(options.layerName)) {
        this.map.addSource(options.layerName, {
          type: 'geojson',
          data: {
            features: options.features,
            type: 'FeatureCollection',
          },
          cluster: true,
          clusterMaxZoom: options.clusterMaxZoom,
        });
      } else {
        this.map.getSource(options.layerName).setData({
          features: options.features,
          type: 'FeatureCollection',
        });
      }

      if (!this.map.getLayer(`${options.layerName}-layer`)) {
        this.map.addLayer({
          id: `${options.layerName}-layer`,
          type: 'symbol',
          minzoom: options.minzoom,
          filter: ['!', ['has', 'point_count']],
          source: options.layerName,
          layout: {
            'icon-image': ['get', 'icon'],
            //'icon-size': .25,
            'icon-offset': [(this.telematicsIconSizes.width / 2) - (this.telematicsIconSizes.width * 0.5), (this.telematicsIconSizes.height / 2) - (this.telematicsIconSizes.height * 0.620155)],
            // 'text-field': ['get', 'licensePlate'],
            // 'text-anchor': 'left',
            // 'text-offset': [1.75, 0],
            // 'text-size': {
            //   stops: [
            //     [16.99999, 0],
            //     [17, 10]
            //   ]
            // },
            'icon-rotate': ['get', 'bearing'],
            'icon-rotation-alignment': 'map',
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
          },
          paint: {
            'text-color': '#333',
          }
        });
      }

      if (!this.map.getLayer(`${options.layerName}-layer-cluster`)) {
        this.map.addLayer({
          id: `${options.layerName}-layer-cluster`,
          type: 'symbol',
          minzoom: options.minzoom,
          filter: ['has', 'point_count'],
          source: options.layerName,
          layout: {
            'icon-image': 'telematics-cluster-icon',
            'icon-size': 1,
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
          }
        });
      }

      if (!this.map.getLayer(`${options.layerName}-layer-bus`)) {
        this.map.addLayer({
          filter: ['!', ['has', 'point_count']],
          id: `${options.layerName}-layer-bus`,
          type: 'symbol',
          minzoom: options.minzoom,
          source: options.layerName,
          layout: {
            'text-field': ['get', 'routeName'],
            'text-size': 10,
            'text-font': ['Open Sans Bold'],
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
          },
          paint: {
            'text-color': '#ffffff',
          }
        });
      }
    },
    delTelematics(params = {}) {
      const defaults = {
        layerName: 'source-telematics',
        features: {...this.telematics}
      }
      const options = {...defaults, ...params};
      if (this.map.getLayer(`${options.layerName}-layer`)) {
        this.map.removeLayer(`${options.layerName}-layer`);
      }
      if (this.map.getLayer(`${options.layerName}-layer-cluster`)) {
        this.map.removeLayer(`${options.layerName}-layer-cluster`);
      }
      if (this.map.getLayer(`${options.layerName}-layer-bus`)) {
        this.map.removeLayer(`${options.layerName}-layer-bus`);
      }
      if (this.map.getSource(`${options.layerName}`)) {
        this.map.removeSource(`${options.layerName}`);
      }
      // this.clearTelematicsStore();
    },
    initTelematics() {
      this.loadTelematicsIcons();
      ['source-telematics-layer', 'current-telematics-layer'].forEach((layer) => {
        this.map.on('mouseenter', layer, e => {
          const item = e.features[0];
          if (item.properties.cluster) {
            return;
          }
          this.map.getCanvas().style.cursor = 'pointer';
          item.layer.telematicsHover = true;
          this.$bus.$emit('showMapHoverPopup', {
            layer: 'editor',
            item,
            containerClassNames: ['editor-popup'],
            popup: {
              closeButton: false
            }
          });
        });
        this.map.on('mouseleave', layer, e => {
          this.map.getCanvas().style.cursor = 'default';
          this.$bus.$emit('hideInfoHoverModal');
        });
      });
    },
    loadTelematicsIcons() {
      const icons = [
        {
          name: 'telematics-icon',
          src: telematicsIconNew,
          sizes: this.telematicsIconSizes
        },
        {
          name: 'telematics-bus-icon-new',
          src: telematicsBusIconNew,
          sizes: this.telematicsIconSizes
        },
        {
          name: 'telematics-trolleybus-icon-new',
          src: telematicsTrolleybusIconNew,
          sizes: this.telematicsIconSizes
        },
        {
          name: 'telematics-tram-icon-new',
          src: telematicsTramIconNew,
          sizes: this.telematicsIconSizes
        },
        {
          name: 'telematics-bus-icon',
          src: telematicsBusIcon,
          sizes: {
            width: 48,
            height: 48,
          }
        },
        {
          name: 'telematics-cluster-icon',
          src: telematicsClusterNewIcon,
          sizes: {
            width: 30,
            height: 30,
          }
        }
      ];
      icons.forEach(icon => {
        const img = new Image(icon.sizes.width, icon.sizes.height);
        img.onload = () => {
          if (this.map) {
            this.map.addImage(icon.name, img);
          }
        };
        img.src = icon.src;
      });

    },
  },
  computed: {
    telematics() {
      return this.$store.state.telematics.telematics;
    }
  }
};
