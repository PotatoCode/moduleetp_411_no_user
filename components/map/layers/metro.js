export const metro = {
  methods: {
    getGradient (color) {
      return {
        name: 'radialGradient',
        stops: [
          { 'offset': '90%', 'stop-color': color },
          { 'offset': '100%', 'stop-color': 'transparent' }
        ]
      }
    },
    genGradientTag(gradient, id = 'gradient') {
      let outerHTML = `<${gradient.name} id="${id}">`
      gradient.stops.forEach( obj => {
        let stopHTML = '\n  <stop';
        for (const attr in obj) {
          stopHTML += ` ${attr}="${obj[attr]}"`;
        }
        stopHTML += ' />';
        outerHTML += stopHTML;
      });
      outerHTML += `\n</${gradient.name}>`;
      return outerHTML;
    },
    addMetroStations() {
      const borderSize = 1;
      const borderColor = '#aaaaaa';
      const haloSize = 3;
      const haloColor = 'white';
      const metroIcons = {};
      const metro = [...this.metroStations];
      const entrances = [...this.entrances];
      metro.concat(entrances).forEach(m => {
        const color = m.properties.color;
        if (!metroIcons[color]) {
          metroIcons[color] = `
                  <svg width="${20 + borderSize + haloSize}px" height="${20 + borderSize + haloSize}px" viewBox="${0 - borderSize - haloSize} ${0 - borderSize - haloSize} ${20 + 2 * (borderSize + haloSize)} ${20 + 2 * (borderSize + haloSize)}" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                      ${this.genGradientTag(this.getGradient(borderColor), 'borderGradient')}
                      ${this.genGradientTag(this.getGradient(haloColor), 'haloGradient')}
                    </defs>
                    <g fill="url(#borderGradient)" fill-rule="nonzero">
                      <circle cx="10" cy="10" r="${10 + borderSize + haloSize}"></circle>
                    </g>
                    <g fill="url(#haloGradient)" fill-rule="nonzero">
                      <circle cx="10" cy="10" r="${10 + haloSize}"></circle>
                    </g>
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g fill="white" fill-rule="nonzero">
                            <circle cx="10" cy="10" r="10" />
                          </g>
                      </g>
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <g fill="${color}" fill-rule="nonzero">
                              <path d="M10,20 C4.4771525,20 0,15.5228475 0,10 C0,4.4771525 4.4771525,0 10,0 C15.5228475,0 20,4.4771525 20,10 C20,15.5228475 15.5228475,20 10,20 Z M12.7583524,4.70444377 L12.6517992,4.70426724 L10.3874994,9.10741759 L8.04172811,4.66666667 L4.83001203,12.6890043 L4,12.6890043 L4,13.3333333 L8.54709977,13.3333333 L8.54709977,12.6890043 L7.64486638,12.6890043 L8.54709977,10.18177 L10.3874994,13.3333333 L12.1558555,10.18177 L13.057911,12.6890043 L12.1558555,12.6890043 L12.1558555,13.3333333 L16.6666667,13.3333333 L16.6666667,12.6890043 L15.8852173,12.6890043 L12.7583524,4.70444377 Z"></path>
                          </g>
                      </g>
                  </svg>`;
        }
      });

      Object.keys(metroIcons).forEach(icon => {
        const img = new Image(100, 100);
        img.onload = () => {
          if (this.map) {
            this.map.addImage(icon, img);
          }
        };
        img.src = `data:image/svg+xml;base64,${window.btoa(metroIcons[icon])}`;
      });

      const mini_icon = `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="125px" height="85.9px" viewBox="337.5 232.3 125 85.9" xml:space="preserve">
        <polygon fill="#FF0013" points="453.9,306.2 424.7,232.3 400,275.5 375.4,232.3 346.1,306.2 337.5,306.2 337.5,317.4 381.7,317.4   381.7,306.2 375.1,306.2 381.5,287.8 400,318.2 418.5,287.8 424.9,306.2 418.3,306.2 418.3,317.4 462.5,317.4 462.5,306.2 "/>
      </svg>`;
      const mini_img = new Image(125, 86);
      mini_img.onload = () => {
        if (this.map) {
          this.map.addImage('metro_mini', mini_img);
        }
      };
      mini_img.src = `data:image/svg+xml;base64,${window.btoa(mini_icon)}`;

      this.map.addLayer({
        id: 'metro',
        type: 'symbol',
        minzoom: 13.5,
        maxzoom: 17,
        source: {
          type: 'geojson',
          data: {
            features: metro,
            type: 'FeatureCollection',
          }
        },
        layout: {
          'icon-allow-overlap': {
            "stops": [
              [0, false],
              [10.5, true]
            ]
          },
          'icon-image': ['get', 'color'],
          'icon-size': .25
        },
      });
      this.map.addLayer({
        id: 'metro-mini',
        type: 'symbol',
        minzoom: 11.5,
        maxzoom: 13.5,
        source: {
          type: 'geojson',
          data: {
            features: metro,
            type: 'FeatureCollection',
          }
        },
        layout: {
          'icon-allow-overlap': false,
          'icon-image': 'metro_mini',
          'icon-size': .125
        },
      });
      if (this.map.getLayer('z-index-100')) {
        this.map.moveLayer('metro', 'z-index-100');
        this.map.moveLayer('metro-mini', 'z-index-100');
      }
      this.map.addLayer({
        id: 'metro-text',
        type: 'symbol',
        minzoom: 13.5,
        maxzoom: 17,
        source: {
          type: 'geojson',
          data: {
            features: metro,
            type: 'FeatureCollection',
          }
        },
        layout: {
          'text-field': ['get', 'name'],
          'text-anchor': 'top',
          'text-offset': [0, 1],
          'text-size': 12
        },
        paint: {
          'text-halo-color': '#fff',
          'text-halo-width': 2
        }
      });
      if (this.map.getLayer('z-index-100')) {
        this.map.moveLayer('metro-text', 'z-index-100');
      }

      this.map.addLayer({
        id: 'metro-entrance',
        type: 'symbol',
        minzoom: 17,
        source: {
          type: 'geojson',
          data: {
            features: entrances,
            type: 'FeatureCollection',
          }
        },
        layout: {
          'icon-allow-overlap': true,
          'icon-image': ['get', 'color'],
          'icon-size': .25
        },
      });
      if (this.map.getLayer('z-index-100')) {
        this.map.moveLayer('metro-entrance', 'z-index-100');
      }
      this.map.addLayer({
        id: 'metro-entrance-text',
        type: 'symbol',
        minzoom: 17,
        source: {
          type: 'geojson',
          data: {
            features: entrances,
            type: 'FeatureCollection',
          }
        },
        layout: {
          'text-allow-overlap': true,
          'text-field': ['get', 'name'],
          'text-anchor': 'top',
          'text-offset': [0, 1],
          'text-size': 14
        },
        paint: {
          'text-halo-color': '#fff',
          'text-halo-width': 2
        }
      });
      if (this.map.getLayer('z-index-100')) {
        this.map.moveLayer('metro-entrance-text', 'z-index-100');
      }
    },
  },
  computed: {
    metroStations() {
      return this.$store.state.metro.metro;
    },
    entrances() {
      return this.$store.state.metro.entrances;
    },
  }
};
