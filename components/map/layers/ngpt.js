import editorData from "./editor";
import {point} from '@turf/helpers';
import connection from '~/assets/js/connection';
import {mapMutations} from 'vuex';

export const ngpt = {
  methods: {
    ...mapMutations({
      disableTelematics: 'telematics/DISABLE'
    }),
    generateEditorStops({routeName}) {
      const stopCounts = {},
            stops = [],
            textIndexes = {}

      let stopIndex = 0

      this.editorData.stopsList[routeName].stops.forEach((stop, i) => {
        let stopData;
        const lastEnd = (this.operator || editorData.viewMode) ? stop.term : i === 0 || (i === this.editorData.stopsList[routeName].stops.length - 1);
        let textInd = (i + 1);
        if (this.operator || editorData.viewMode) {
          textInd = stop.sequenceID;
        }
        if (stop && stop.coordinates && stop.type === 'user') { // userStop
          stopData = point(stop.coordinates);
          stopData.id = stop.id === '' ? `newStop${i + 1}` : stop.id;
          stopData.properties = {
            stopType: 'user',
            icon: stopData.id === this.editorData.highLightStop ? 'ngpt-coral' : 'ngpt-green',
            name: stop.text,
            ind: i,
            textInd,
            textIndSize: lastEnd ? 12 : 10,
            routeName,
            enabled: stop.enabled ? 1 : 0,
            iconSize: lastEnd ? .4 : .3,
            id: stopData.id,
          };
          if (editorData.highLightStop !== stop.id && lastEnd) {
            stopData.properties.icon = 'bus_edge';
          }
        } else if (stop && stop.type === 'master' && stop.value && (this.stopsById[stop.value] || this.allStopsById[stop.value])) { // masterStop
          let textInd = (i + 1);
          if (this.operator || editorData.viewMode) {
            textInd = stop.old ? '' : stop.sequenceID;
          }
          stop.sequenceID = textInd;
          stopData = {...this.stopsById[stop.value] || this.allStopsById[stop.value]};
          this.stopsById[stop.value].sequenceID = textInd;
          stopData.properties = {
            stopType: 'master',
            icon: stop.value === this.editorData.highLightStop ? 'ngpt-coral' : `ngpt-${routeName}-icon`,
            name: stop.text,
            ind: i,
            id: stop.value,
            textInd,
            textIndSize: lastEnd ? 12 : 10,
            routeName,
            iconSize: lastEnd ? .4 : .3,
            old: stop.old
          };
          if ((this.operator || editorData.viewMode) && this.proposal.routeID && stop.new && stop.value !== this.editorData.highLightStop) {
            stopData.properties.icon = 'ngpt-new';
          }
          if (editorData.highLightStop !== stop.value && lastEnd) {
            stopData.properties.icon = 'bus_edge';
          }
          if ((this.operator || editorData.viewMode) && this.proposal.routeID && stop.old && stop.value !== this.editorData.highLightStop) {
            stopData.properties.icon = 'ngpt-red';
            delete stopData.properties.textInd;
          }
        }

        // if (stopData) {
        //   stops.push(stopData);
        //   if (stop) {
        //     const id = stopData.properties.id;
        //     if (stopCounts[id]) {
        //       if (stopCounts[id].properties.old) {
        //         stopCounts[id].properties.textInd = '';
        //       } else {
        //         const textInds = [];
        //         const dupe = stops.filter((s) => {return s.properties.id === stopData.properties.id});
        //         dupe.forEach((s) => {
        //           textInds.push(s.properties.textInd);
        //           s.properties.textInd = ' ';
        //         });
        //         if (stopCounts[id].properties.textInd) {
        //           if (stopData.properties.icon !== 'ngpt-coral') {
        //             stopData.properties.textInd = textInds.join(',');
        //             stopCounts[id].properties.textInd = '';
        //             const key = stopData.properties.type === 'master' ? 'value' : 'id';
        //             const stopInStopList = this.editorData.stopsList[routeName].stops.find((s) => {return s[key] === id});
        //             if (stopInStopList) {
        //               if (stopInStopList.sequenceID !== 1 && stopInStopList.sequenceID !== this.editorData.stopsList[routeName].stops.length) {
        //                 stopData.properties.icon = `ngpt-${routeName}-icon`;
        //               }
        //               else {
        //                 stopData.properties.icon = 'bus_edge';
        //               }
        //             }
        //           } else {
        //             stopData.properties.textInd = '';
        //             stopCounts[id].properties.textInd = textInds.join(',');
        //             if (stopCounts[id].properties.iconSize === .4) {
        //               stopData.properties.iconSize = .4;
        //             }
        //           }
        //         } else {
        //           const prev = stopCounts[id].properties.textInd;
        //           stopCounts[id].properties.textInd = '';
        //           stopCounts[id] = stopData;
        //           stopData.properties.textInd = `${prev},${i + 1}`;
        //         }
        //       }
        //       stopCounts[id].properties.textIndSize -= 1;
        //     } else {
        //       stopCounts[id] = stopData;
        //     }
        //   }
        // }

        if (stopData) {

          stops.push(stopData);

          let id = stopData.properties.id

          if (stopData.properties.old) {
            textIndexes[id] = ''
            stopIndex--
          } else {
            textIndexes[id] 
              ? textIndexes[id] += `,${stopIndex + 1}` 
              : textIndexes[id] = `${stopIndex + 1}`
          }
          if (!stopCounts[id]) stopCounts[id] = stopData

        }

        stopIndex++
        
      });

      stops.forEach((s) => s.properties.textInd = textIndexes[s.properties.id])

      if (editorData.viewMode && this.proposal.routeID) {
        let ind = 0;
        return stops.map(stop => {
          if (!stop.properties.old && !stopCounts[stop.properties.id]) {
            ind += 1;
            stop.properties.textInd = ind;
          }
          return stop;
        });
      }

      if (this.proposal) {
        if (this.proposal.routeMainType === 2 && stops.length > 0) {
          const last = stops.length - 1;
          if (stops[0].properties.icon !== 'ngpt-coral') {
            stops[0].properties.icon = 'bus_edge';
          }
          if (stops[last].properties.icon !== 'ngpt-coral') {
            stops[last].properties.icon = 'bus_edge';
          }
        }
      }

      return stops;
    },
    redrawCurrentTS() {
      if (this.$nuxt.$store.state.telematics.available) {
        const polyline = require('@mapbox/polyline');
        this.editorData.currentTS.status = 'ready';
        const arr = [];
        if (Object.keys(this.editorData.currentTS.features).length > 0) {
          Object.values(this.editorData.currentTS.features).filter((f) => {
            return this.editorData.currentTS.routes[f.routeId] !== undefined;
          }).forEach((f) => {
            polyline.decode(f.geom).forEach((p) => {
              arr.push({
                type: "Feature",
                geometry: {
                  type: "Point",
                  coordinates: p
                },
                properties: f
              })
            })
          });
          this.editorData.currentTS.featuresToDraw = [...arr];
          this.editorData.currentTS.count = arr.length;
          if (this.editorData.currentTS.show) {
            this.$bus.$emit('addTelematics', {layerName: 'current-telematics', features: [...this.editorData.currentTS.featuresToDraw], minzoom: 12, clusterMaxZoom: 18});
          }
        }
        else if (this.editorData.currentTS.featuresToDraw.length > 0) {
          const arr = [...this.editorData.currentTS.featuresToDraw].filter((f) => {
            return this.editorData.currentTS.routes[f.properties.routeId] !== undefined;
          })
          this.editorData.currentTS.featuresToDraw = [...arr];
          this.editorData.currentTS.count = this.editorData.currentTS.featuresToDraw.length;
          if (this.editorData.currentTS.show) {
            this.$bus.$emit('addTelematics', {layerName: 'current-telematics', features: [...this.editorData.currentTS.featuresToDraw], minzoom: 12, clusterMaxZoom: 18});
          }
        }
      }
    },
    async updateCurrentTSInfo(stops) {
      if (this.$nuxt.$store.state.telematics.available) {
        if (this.$nuxt.$route.query.viewMode) {
          if (this.$nuxt.$store.state.proposals.proposal.routeMainType === 2) {
            return;
          }
        }
        else if (this.$nuxt.$route.query.cablewayEdit) {
          return;
        }
        // this.editorData.currentTS.features = {};
        this.editorData.currentTS.status = 'loading';
        let routeID = null;
        if (this.$store.state.proposals.proposal.routeID) {
          routeID = this.$store.state.proposals.proposal.routeID;
        }
        else {
          if (this.routeID !== undefined && this.routeID !== null) {
            routeID = this.routeID;
          }
        }
        if (routeID !== null && routeID.toString() === "0") {
          routeID = null;
        }
        if (routeID) {
          const vehicleType = this.$store.state.ngpt.routesByID.length === 0 ? 'А' : this.$store.state.ngpt.routesByID[0].properties.vehicleType;
          const icon = vehicleType === 'А' ? 'telematics-bus-icon-new' :
            vehicleType === 'Тб' ? 'telematics-trolleybus-icon-new' :
            'telematics-tram-icon-new';
          if (!this.editorData.currentTS.routes[routeID]) {
            this.editorData.currentTS.routes[routeID] = true;
            const self = this;
            this.$axios.get(
              `${connection.getAddress({version: 'v0.7.1'})}/moscow/vehicles/telematics/byRouteID/${routeID}`
            ).then((ts) => {
              if (ts.status >= 200 && ts.status < 300) {
                ts.data.data.forEach((d) => {
                  d.icon = icon
                  self.editorData.currentTS.devices[d.device_id] = d;
                })
              }
              Object.values(self.editorData.currentTS.devices).forEach((device) => {
                self.editorData.currentTS.features[device.device_id] = device
              })
              self.redrawCurrentTS();
            })
          }
          else {
            this.editorData.currentTS.status = 'ready';
          }
        }
        else {
          this.editorData.currentTS.routes = {};
          this.editorData.currentTS.devices = {};
          const stopIDForQuery = [];
          stops.forEach((stop) => {
            const stopID = stop.properties.stopID || stop.id;
            if (stopID !== undefined) {
              if (this.editorData.currentTS.routesByStopID[stopID] === undefined) {
                stopIDForQuery.push(stopID)
              }
              else {
                const routes = this.editorData.currentTS.routesByStopID[stopID];
                routes.forEach((route) => {
                  this.editorData.currentTS.routes[route.routeID] = true;
                })
              }
            }
          })
          if (stopIDForQuery.length !== 0) {
            const promise = [];
            stopIDForQuery.forEach((id) => {
              promise.push(this.$axios.get(
                `${connection.getAddress({version: 'v0.7.1'})}/moscow/ngpt/stops/infoByStopID/${id}`
              ))
            })
            const self = this;
            Promise.all(promise).then((results) => {
              const tsPromise = [];
              results.forEach((result) => {
                if (result.status >= 200 && result.status < 300) {
                  self.editorData.currentTS.routesByStopID[result.data[0].stopID] = result.data;
                  result.data.forEach((route) => {
                    self.editorData.currentTS.routes[route.routeID] = true;
                  })
                }
              })
              Object.keys(self.editorData.currentTS.routes).forEach((route) => {
                tsPromise.push(self.$axios.get(
                  `${connection.getAddress({version: 'v0.7.1'})}/moscow/vehicles/telematics/byRouteID/${route}`
                ))
              })
              Promise.all(tsPromise).then((tsResults) => {
                tsResults.forEach((ts) => {
                  if (ts.status >= 200 && ts.status < 300) {
                    // self.editorData.currentTS.features = [...self.editorData.currentTS.features, ...ts.data.data];
                    ts.data.data.forEach((d) => {
                      const routes = this.$store.state.ngpt.routes.filter((r) => {return r.routeID === d.routeId});
                      const vehicleType = routes.length === 0 ? 'А' : routes[0].vehicleType;
                      const icon = vehicleType === 'А' ? 'telematics-bus-icon-new' :
                        vehicleType === 'Тб' ? 'telematics-trolleybus-icon-new' :
                        'telematics-tram-icon-new';
                      d.icon = icon;
                      self.editorData.currentTS.devices[d.device_id] = d;
                    })
                  }
                })
                Object.values(self.editorData.currentTS.devices).forEach((device) => {
                  self.editorData.currentTS.features[device.device_id] = device;
                })
                self.redrawCurrentTS();
              })
            })
            .catch((err) => {
              console.error(err);
              self.editorData.currentTS.status = 'error';
              this.disableTelematics();
              this.$snotify.error(
                'Данные о телематике недоступны',
                {
                  position: 'centerTop',
                  titleMaxLength: 100
                }
              );
            })
          }
          else {
            this.redrawCurrentTS();
          }
        }
      }
    },
    async addEditorStops(params) {
      const mainRouteStops = this.generateEditorStops({routeName: 'mainRoute'});
      const backRouteStops = this.generateEditorStops({routeName: 'backRoute'});

      if (!params || !params.skipLines) {
        this.addEditorLines(mainRouteStops, backRouteStops, params);
      }

      let stops = [].concat(mainRouteStops).concat(backRouteStops).map((s, i) => {
        return s;
      });

      // AISMS-1080: этот блок кода повторно пушит в stops
      // новые, добавленные пользователем остановки - но без поля textInd.

      // Object.values(this.editorData.userStopsById).forEach(stop => { // user stops
      //   const userStop = point(stop.coordinates);
      //   userStop.id = stop.id;
      //   userStop.properties = {
      //     stopType: 'user',
      //     icon: 'ngpt-green',
      //     iconImage: 'local-ngpt-green',
      //     name: stop.text,
      //     enabled: 0,
      //     opacity: 0.5,
      //     iconSize: .3,
      //     id: stop.id
      //   };
      //   stops.push(userStop);
      // });

      if (editorData.newUserStop) {
        stops.push(editorData.newUserStop);
      }

      const stopsForCurrentTS = [];
      let flag = true;
      if (params) {
        if (params.skipLines || params.skipSaveHistory) {
          flag = false;
        }
      }
      if (flag) {
        stops.forEach((stop) => {
          if (stop.properties.stopType === 'master') {
            stopsForCurrentTS.push(this.$store.state.ngpt.stopsById[stop.id]);
          }
        })
        if (params) {
          if (!params.noUpdateLineGeometry) {
            this.updateCurrentTSInfo([...stopsForCurrentTS]);
          }
        }
        else {
          this.updateCurrentTSInfo([...stopsForCurrentTS]);
        }
      }

      const colors = {
        'ngpt-mainRoute-icon': '#0080ff',
        'ngpt-backRoute-icon': '#008000',
        'ngpt-green': '#09ba92',
        'bus_edge': '#163354'
      };

      for (let i = 0; i < stops.length; i++) {
        if (colors[stops[i].properties.icon] && stops[i].properties.textInd) {
          let textIndex = stops[i].properties.textInd
          await this.addNgptIcon({
            text: /^,+/.test(textIndex) ? textIndex.replace(/^,+/g, '') : textIndex,
            textSize: 10.4 - stops[i].properties.textInd.toString().length,
            circleGradient: this.getGradient(colors[stops[i].properties.icon]),
            name: `local-ngpt-icon-${i}`,
            wh: 85, ...this.borderAndHalo
          });
          stops[i].properties.iconImage = `local-ngpt-icon-${i}`;
        } else if (!stops[i].properties.iconImage) {
          stops[i].properties.iconImage = stops[i].properties.icon;
        }
      }

      if (!this.map.getSource('source-editor-stops')) {
        this.map.addSource('source-editor-stops', {
          type: 'geojson',
          data: {
            features: stops,
            type: 'FeatureCollection',
          }
        });
      } else {
        this.map.getSource('source-editor-stops').setData({
          features: stops,
          type: 'FeatureCollection',
        });
      }

      if (!this.map.getLayer('layer-editor-stops-main-route')) {
        this.map.addLayer({
          minzoom: 13,
          id: 'layer-editor-stops-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-mainRoute-icon'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': ['get', 'iconImage'],
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-stops_mini-main-route')) {
        this.map.addLayer({
          minzoom: 11.2,
          maxzoom: 13,
          id: 'layer-editor-stops_mini-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-mainRoute-icon'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'ngpt-mainRoute-mini',
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-deleted-stops-main-route')) {
        this.map.addLayer({
          minzoom: 13,
          id: 'layer-editor-deleted-stops-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-red'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-red',
            'icon-size': ['get', 'iconSize'],
            'text-field': ['get', 'textInd'],
            'text-size': ['get', 'textIndSize'],
            'icon-allow-overlap': true,
            'text-allow-overlap': true,
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-deleted-stops_mini-main-route')) {
        this.map.addLayer({
          minzoom: 11.2,
          maxzoom: 13,
          id: 'layer-editor-deleted-stops_mini-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-red'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-red-mini',
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-green-stops-main-route')) {
        this.map.addLayer({
          minzoom: 13,
          id: 'layer-editor-green-stops-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-green'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': ['get', 'iconImage'],
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-green-stops_mini-main-route')) {
        this.map.addLayer({
          minzoom: 11.2,
          maxzoom: 13,
          id: 'layer-editor-green-stops_mini-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-green'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-green-mini',
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-new-stops-main-route')) {
        this.map.addLayer({
          minzoom: 13,
          id: 'layer-editor-new-stops-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-new'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-new',
            'icon-size': ['get', 'iconSize'],
            'text-field': ['get', 'textInd'],
            'text-size': ['get', 'textIndSize'],
            'icon-allow-overlap': true,
            'text-allow-overlap': true,
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-new-stops_mini-main-route')) {
        this.map.addLayer({
          minzoom: 11.2,
          maxzoom: 13,
          id: 'layer-editor-new-stops_mini-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-new'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-new-mini',
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-edge-stops-main-route')) {
        this.map.addLayer({
          minzoom: 13,
          id: 'layer-editor-edge-stops-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'bus_edge']
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': ['get', 'iconImage'],
            'icon-size': .4,
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-410');
      }

      if (!this.map.getLayer('layer-editor-edge_mini-stops-main-route')) {
        this.map.addLayer({
          minzoom: 10,
          maxzoom: 13,
          id: 'layer-editor-edge_mini-stops-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'bus_edge']
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': ['get', 'iconImage'],
            'icon-size': .3,
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-410');
      }

      if (!this.map.getLayer('layer-editor-highlight-stops-main-route')) {
        this.map.addLayer({
          minzoom: 10,
          id: 'layer-editor-highlight-stops-main-route',
          filter: [
            'all',
            ['==', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-coral']
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-coral',
            'icon-size': ['get', 'iconSize'],
            'text-field': ['get', 'textInd'],
            'text-size': ['get', 'textIndSize'],
            'icon-allow-overlap': true,
            'text-allow-overlap': true,
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-410');
      }

      if (!this.map.getLayer('layer-editor-stops-back-route')) {
        this.map.addLayer({
          minzoom: 13,
          id: 'layer-editor-stops-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-backRoute-icon'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': ['get', 'iconImage'],
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-stops_mini-back-route')) {
        this.map.addLayer({
          maxzoom: 13,
          minzoom: 11.2,
          id: 'layer-editor-stops_mini-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-backRoute-icon'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'ngpt-backRoute-mini',
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-deleted-stops-back-route')) {
        this.map.addLayer({
          minzoom: 13,
          id: 'layer-editor-deleted-stops-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-red'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-red',
            'icon-size': ['get', 'iconSize'],
            'text-field': ['get', 'textInd'],
            'text-size': ['get', 'textIndSize'],
            'icon-allow-overlap': true,
            'text-allow-overlap': true,
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-deleted-stops_mini-back-route')) {
        this.map.addLayer({
          minzoom: 11.2,
          maxzoom: 13,
          id: 'layer-editor-deleted-stops_mini-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-red'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-red-mini',
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-green-stops-back-route')) {
        this.map.addLayer({
          minzoom: 13,
          id: 'layer-editor-green-stops-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-green'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': ['get', 'iconImage'],
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-390');
      }

      if (!this.map.getLayer('layer-editor-green-stops_mini-back-route')) {
        this.map.addLayer({
          minzoom: 11.2,
          maxzoom: 13,
          id: 'layer-editor-green-stops_mini-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-green'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-green-mini',
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-new-stops-back-route')) {
        this.map.addLayer({
          minzoom: 13,
          id: 'layer-editor-new-stops-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-new'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-new',
            'icon-size': ['get', 'iconSize'],
            'text-field': ['get', 'textInd'],
            'text-size': ['get', 'textIndSize'],
            'icon-allow-overlap': true,
            'text-allow-overlap': true,
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-new-stops_mini-back-route')) {
        this.map.addLayer({
          minzoom: 11.2,
          maxzoom: 13,
          id: 'layer-editor-new-stops_mini-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-new'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-new-mini',
            'icon-size': ['get', 'iconSize'],
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          }
        }, 'z-index-400');
      }

      if (!this.map.getLayer('layer-editor-edge-stops-back-route')) {
        this.map.addLayer({
          minzoom: 13,
          id: 'layer-editor-edge-stops-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'bus_edge'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': ['get', 'iconImage'],
            'icon-size': .4,
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-410');
      }

      if (!this.map.getLayer('layer-editor-edge_mini-stops-back-route')) {
        this.map.addLayer({
          minzoom: 10,
          maxzoom: 13,
          id: 'layer-editor-edge_mini-stops-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'bus_edge'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': ['get', 'iconImage'],
            'icon-size': .3,
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-410');
      }

      if (!this.map.getLayer('layer-editor-highlight-stops-back-route')) {
        this.map.addLayer({
          minzoom: 10,
          id: 'layer-editor-highlight-stops-back-route',
          filter: [
            'all',
            ['!=', ['get', 'routeName'], 'mainRoute'],
            ['==', ['get', 'icon'], 'ngpt-coral'],
          ],
          type: 'symbol',
          source: 'source-editor-stops',
          layout: {
            'icon-image': 'local-ngpt-coral',
            'icon-size': ['get', 'iconSize'],
            'text-field': ['get', 'textInd'],
            'text-size': ['get', 'textIndSize'],
            'icon-allow-overlap': true,
            'text-allow-overlap': true,
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
            'symbol-z-order': 'source'
          },
          paint: {
            'text-color': '#fff'
          }
        }, 'z-index-410');
      }

      if (editorData.activeRoute === 'mainRoute') {
        this.map.moveLayer('layer-editor-stops-back-route', 'layer-editor-stops-main-route');
        this.map.moveLayer('layer-editor-stops_mini-back-route', 'layer-editor-stops_mini-main-route');
        this.map.moveLayer('layer-editor-deleted-stops-back-route', 'layer-editor-deleted-stops-main-route');
        this.map.moveLayer('layer-editor-deleted-stops_mini-back-route', 'layer-editor-deleted-stops_mini-main-route');
        this.map.moveLayer('layer-editor-green-stops-back-route', 'layer-editor-green-stops-main-route');
        this.map.moveLayer('layer-editor-green-stops_mini-back-route', 'layer-editor-green-stops_mini-main-route');
        this.map.moveLayer('layer-editor-new-stops-back-route', 'layer-editor-new-stops-main-route');
        this.map.moveLayer('layer-editor-new-stops_mini-back-route', 'layer-editor-new-stops_mini-main-route');
        this.map.moveLayer('layer-editor-edge-stops-back-route', 'layer-editor-edge-stops-main-route');
        this.map.moveLayer('layer-editor-edge_mini-stops-back-route', 'layer-editor-edge_mini-stops-main-route');
        this.map.moveLayer('layer-editor-highlight-stops-back-route', 'layer-editor-highlight-stops-main-route');
      } else {
        this.map.moveLayer('layer-editor-stops-main-route', 'layer-editor-stops-back-route');
        this.map.moveLayer('layer-editor-stops_mini-main-route', 'layer-editor-stops_mini-back-route');
        this.map.moveLayer('layer-editor-deleted-stops-main-route', 'layer-editor-deleted-stops-back-route');
        this.map.moveLayer('layer-editor-deleted-stops_mini-main-route', 'layer-editor-deleted-stops_mini-back-route');
        this.map.moveLayer('layer-editor-green-stops-main-route', 'layer-editor-green-stops-back-route');
        this.map.moveLayer('layer-editor-green-stops_mini-main-route', 'layer-editor-green-stops_mini-back-route');
        this.map.moveLayer('layer-editor-new-stops-main-route', 'layer-editor-new-stops-back-route');
        this.map.moveLayer('layer-editor-new-stops_mini-main-route', 'layer-editor-new-stops_mini-back-route');
        this.map.moveLayer('layer-editor-edge-stops-main-route', 'layer-editor-edge-stops-back-route');
        this.map.moveLayer('layer-editor-edge_mini-stops-main-route', 'layer-editor-edge_mini-stops-back-route');
        this.map.moveLayer('layer-editor-highlight-stops-main-route', 'layer-editor-highlight-stops-back-route');
      }

      if (!this.map.getLayer('layer-editor-stops-name')) {
        this.map.addLayer({
          id: 'layer-editor-stops-name',
          type: 'symbol',
          source: 'source-editor-stops',
          minzoom: 16,
          layout: {
            'text-field': ['get', 'name'],
            'text-anchor': 'left',
            'text-offset': [1, 0],
            'text-size': 15,
            'text-allow-overlap': true,
          },
          paint: {
            'text-halo-color': '#fff',
            'text-halo-width': 2
          }
        }, 'z-index-400');
      }
    },
    async addEditorLines(mainRouteStops, backRouteStops, params) {
      editorData.routeProperties.stopsNum.value = mainRouteStops.filter(s => s && !s.properties.old).length + backRouteStops.filter(s => s && !s.properties.old).length;

      if (mainRouteStops.length) {
        editorData.routeProperties.from.value = mainRouteStops[0].properties.name;
      } else {
        editorData.routeProperties.from.value = null;
      }

      if (mainRouteStops.length > 1) {
        editorData.routeProperties.to.value = mainRouteStops[mainRouteStops.length - 1].properties.name;
      } else {
        editorData.routeProperties.to.value = null;
      }

      let mainRouteLines;
      let backRouteLines;

      const mainRouteLine = await this.getRouteLine({
        stops: mainRouteStops,
        routeName: 'mainRoute',
        ...params
      });
      if (mainRouteLine) {
        mainRouteLines = [mainRouteLine];
      }
      const backRouteLine = await this.getRouteLine({
        stops: backRouteStops,
        routeName: 'backRoute',
        ...params,
      });

      if (backRouteLine) {
        backRouteLines = [backRouteLine];
      }

      if (!(params && params.fromHistory) && !(params && params.skipSaveHistory)) {
        this.saveHistory();
      }

      if (params && params.noRenderLines) {
        return;
      }

      if (mainRouteLine && backRouteLine) {
        const districts = (mainRouteLine.properties.districts || []).concat(backRouteLine.properties.districts || []);
        const divisions = (mainRouteLine.properties.divisions || []).concat(backRouteLine.properties.divisions || []);
        const metroStations = (mainRouteLine.properties.metroStations || []).concat(backRouteLine.properties.metroStations || []);

        editorData.routeProperties.districts = [...new Set(districts)];
        editorData.routeProperties.divisions = [...new Set(divisions)];
        editorData.routeProperties.metroStations = [...new Set(metroStations)];
      }

      let mainRouteLength = mainRouteLines ? (mainRouteLines.reduce((acc, curr) => acc + curr.distance, 0) / 1000).toFixed(1) : null;
      let backRouteLength = backRouteLines ? (backRouteLines.reduce((acc, curr) => acc + curr.distance, 0) / 1000).toFixed(1) : null;

      if (isNaN(mainRouteLength)) {
        mainRouteLength = null;
      }
      if (isNaN(backRouteLength)) {
        backRouteLength = null;
      }

      editorData.routeProperties.mainRouteLengthKm.value = mainRouteLength;
      editorData.routeProperties.backRouteLengthKm.value = backRouteLength;
      editorData.routeProperties.lengthKm.value = +mainRouteLength + +backRouteLength ? (+mainRouteLength + +backRouteLength).toFixed(1) : null;

      let mainRouteTime = mainRouteLines ? Math.floor(mainRouteLines.reduce((acc, curr) => acc + curr.duration, 0) / 60) : null;
      let backRouteTime = backRouteLines ? Math.floor(backRouteLines.reduce((acc, curr) => acc + curr.duration, 0) / 60) : null;

      if (isNaN(mainRouteTime)) {
        mainRouteTime = null;
      }
      if (isNaN(backRouteTime)) {
        backRouteTime = null;
      }

      editorData.routeProperties.mainRouteTime.value = mainRouteTime;
      editorData.routeProperties.backRouteTime.value = backRouteTime;

      let mainRouteSpeed = mainRouteLines ? (mainRouteLines.reduce((acc, curr) => acc + curr.speed, 0)).toFixed(1) : null;
      let backRouteSpeed = backRouteLines ? (backRouteLines.reduce((acc, curr) => acc + curr.speed, 0)).toFixed(1) : null;

      if (isNaN(mainRouteSpeed)) {
        mainRouteSpeed = null;
      }
      if (isNaN(backRouteSpeed)) {
        backRouteSpeed = null;
      }

      editorData.routeProperties.mainRouteSpeed.value = mainRouteSpeed;
      editorData.routeProperties.backRouteSpeed.value = backRouteSpeed;

      editorData.routeProperties.time.value = null;
      if (typeof mainRouteTime === 'number') {
        editorData.routeProperties.time.value = +mainRouteTime;
      }
      if (typeof backRouteTime === 'number') {
        if (typeof editorData.routeProperties.time.value === 'number') {
          editorData.routeProperties.time.value += +backRouteTime;
        } else {
          editorData.routeProperties.time.value = +backRouteTime;
        }
      }

      let intervalMinutesValue = this.route ? Math.round(1 / this.route.vehiclesNum * editorData.routeProperties.lengthKm.value / 16.6 * 60) : null;
      if (intervalMinutesValue === Infinity) {
        intervalMinutesValue = "-"
      }
      editorData.routeProperties.intervalMinutes.value = intervalMinutesValue;

      let features = [];

      if (editorData.activeRoute === 'mainRoute') {
        if (backRouteLines) {
          features = features.concat(backRouteLines);
        }
        if (mainRouteLines) {
          features = features.concat(mainRouteLines);
        }
      }

      if (editorData.activeRoute === 'backRoute') {
        if (mainRouteLines) {
          features = features.concat(mainRouteLines);
        }
        if (backRouteLines) {
          features = features.concat(backRouteLines);
        }
      }

      features = features.map(feature => ({
        type: 'Feature',
        id: feature.properties.id,
        geometry: feature.geometry,
        properties: feature.properties
      }));

      let masterLines = [];
      if (this.routeID && editorData.showOriginalMasterCopy) {
        const masterLine = this.$store.state.ngpt.routesByID.find(r => (r.properties.routeID === this.routeID) && (r.properties.dir === (editorData.activeRoute === 'mainRoute' ? 0 : 1)));
        if (masterLine) {
          masterLines.push({
            ...masterLine,
            properties: {
              ...masterLine.properties,
              color: 'gray'
            }
          })
        }
      }

      if (!this.map.getSource('source-editor-master-lines')) {
        this.map.addSource('source-editor-master-lines', {
          type: 'geojson',
          data: {
            features: masterLines,
            type: 'FeatureCollection',
          }
        });
      } else {
        this.map.getSource('source-editor-master-lines').setData({
          features: masterLines,
          type: 'FeatureCollection',
        });
      }

      if (!this.map.getSource('source-editor-lines')) {
        this.map.addSource('source-editor-lines', {
          type: 'geojson',
          data: {
            features,
            type: 'FeatureCollection',
          }
        });
      } else {
        this.map.getSource('source-editor-lines').setData({
          features,
          type: 'FeatureCollection',
        });
      }

      if (!this.map.getLayer('layer-editor-lines-halo-border')) {
        this.map.addLayer({
          id: 'layer-editor-lines-halo-border',
          type: 'line',
          source: 'source-editor-lines',
          paint: {
            'line-color': '#aaa',
            'line-width': 10,
          },
          layout: {
            'line-join': 'round',
            'line-cap': 'round'
          },
        }, 'z-index-300');
      }

      if (!this.map.getLayer('layer-editor-lines-halo')) {
        this.map.addLayer({
          id: 'layer-editor-lines-halo',
          type: 'line',
          source: 'source-editor-lines',
          paint: {
            'line-color': '#fff',
            'line-width': 8,
          },
          layout: {
            'line-join': 'round',
            'line-cap': 'round'
          },
        }, 'z-index-300');
      }

      if (!this.map.getLayer('layer-editor-lines')) {
        this.map.addLayer({
          id: 'layer-editor-lines',
          type: 'line',
          source: 'source-editor-lines',
          paint: {
            'line-color': ['get', 'color'],
            'line-width': 4,
          },
          layout: {
            'line-join': 'round',
            'line-cap': 'round'
          },
        }, 'z-index-300');
      }

      if (!this.map.getLayer('layer-editor-master-lines')) {
        this.map.addLayer({
          id: 'layer-editor-master-lines',
          type: 'line',
          source: 'source-editor-master-lines',
          paint: {
            'line-color': ['get', 'color'],
            'line-width': 6,
            'line-dasharray': [2, 2]
          }
        }, 'z-index-300');
      }

      this.startRoutePropertiesInit();
      if (!this.routeID && !this.cablewayEdit && !(params && params.skipSaveHistory)) {
        this.$bus.$emit('updateSimilarRoutes', {mainRouteStops, backRouteStops, mainRouteLine, backRouteLine});
      }
    },
    addAllStops() {
      const allStops = [];
      const stopsIds = Object.values(this.editorData.stopsList).reduce((acc, curr) => {
        curr.stops.forEach(s => {
          acc[s.id] = true;
        });
        return acc;
      }, {});
      this.stops.forEach(stop => {
        if (!stopsIds[stop.id]) {
          allStops.push({
            ...stop,
            properties: {
              ...stop.properties,
              icon: 'ngpt-gray',
              id: stop.id,
            }
          });
        }
      });

      if (!this.map.getSource('source-editor-allStops')) {
        this.map.addSource('source-editor-allStops', {
          type: 'geojson',
          data: {
            features: allStops,
            type: 'FeatureCollection',
          }
        });
      } else {
        this.map.getSource('source-editor-allStops').setData({
          features: allStops,
          type: 'FeatureCollection',
        });
      }

      if (!this.map.getLayer('layer-editor-allStops')) {
        this.map.addLayer({
          minzoom: 15.25,
          id: 'layer-editor-allStops',
          type: 'symbol',
          source: 'source-editor-allStops',
          layout: {
            'icon-image': 'ngpt-gray-icon',
            'icon-size': .7,
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
          },
          paint: {
            'icon-opacity': .5,
          }
        }, 'z-index-200');
      }

      if (!this.map.getLayer('layer-editor-allStops_mini')) {
        this.map.addLayer({
          maxzoom: 15.25,
          minzoom: 13,
          id: 'layer-editor-allStops_mini',
          type: 'symbol',
          source: 'source-editor-allStops',
          layout: {
            'icon-image': 'ngpt-mini',
            'icon-size': .3,
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
          },
        }, 'z-index-200');
      }

      if (!this.map.getLayer('layer-editor-allStops-names')) {
        this.map.addLayer({
          minzoom: 16,
          id: 'layer-editor-allStops-names',
          type: 'symbol',
          source: 'source-editor-allStops',
          layout: {
            'text-field': ['get', 'name'],
            'text-anchor': 'left',
            'text-offset': [1, 0],
            'text-size': 10,
          },
          paint: {
            'text-opacity': .5,
            'text-halo-color': '#fff',
            'text-halo-width': 2
          }
        }, 'z-index-200');
      }
    },
  },
  computed: {
    cablewayEdit() {
      return this.$store.state.editor.cablewayEdit;
    },
    stopsById() {
      return this.$store.state.ngpt.stopsById;
    },
    allStopsById() {
      return this.$store.state.ngpt.allStopsById;
    },
  }
};
