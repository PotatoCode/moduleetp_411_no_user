import jdIcon from '~/assets/img/jd.svg';

export const jd = {
  methods: {
    addJdStations() {
      this.map.addLayer({
        id: 'jd',
        type: 'symbol',
        minzoom: 13.5,
        source: {
          type: 'geojson',
          data: {
            features: this.jdStations,
            type: 'FeatureCollection',
          }
        },
        layout: {
          'icon-image': 'jd-icon',
          'icon-size': .25,
          'icon-allow-overlap': true,
          'icon-ignore-placement': true,
        },
      });
      this.map.addLayer({
        id: 'jd-name',
        type: 'symbol',
        minzoom: 13.5,
        source: {
          type: 'geojson',
          data: {
            features: this.jdStations,
            type: 'FeatureCollection',
          }
        },
        layout: {
          'text-field': ['get', 'name'],
          'text-anchor': 'top',
          'text-offset': ['get', 'textOffset'],
          'text-size': {
            "stops": [
              [0, 0],
              [14.49999, 0],
              [14.5, 12]
            ]
          }
        },
        paint: {
          'text-halo-color': '#fff',
          'text-halo-width': 2
        }
      });
      if (this.map.getLayer('z-index-100')) {
        this.map.moveLayer('jd', 'z-index-100');
      }
    },
    loadJdIcon() {
      const img = new Image(80, 80);
      img.onload = () => {
        if (this.map) {
          this.map.addImage('jd-icon', img);
        }
      };
      img.src = jdIcon;
    }
  },
  computed: {
    jdStations() {
      return this.$store.state.jd.stops.map(s => ({
        ...s,
        properties: {
          ...s.properties,
          textOffset: s.properties.mcdNameLatin ? [1.75, 1] : [0, 1]
        }
      }));
    }
  }
};
