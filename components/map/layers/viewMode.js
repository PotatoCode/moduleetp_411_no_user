import editorData from "./editor";
import {multiLineString, lineString} from '@turf/helpers';
import length from '@turf/length';

export const viewMode = {
  methods: {
    editorInitViewMode() {
      try {
        if (this.proposalInfo.routeMainType === 2) {

          this.addMetroStations();
          this.addJdStations();
          this.addMcdStations();
          this.addAllStops();

          /* fill route data */
          let counter = 0;
          const mainRouteStops = [];
          if (this.proposalInfo.forwardStopsParams) {
            this.proposalInfo.forwardStopsParams.stopsNew.forEach(s => {
              mainRouteStops.push({
                coordinates: s.coordinates,
                enabled: true,
                id: s.id || `cableway${counter++}`,
                new: true,
                old: false,
                sequenceID: s.sequenceID,
                text: s.name,
                type: 'user',
                value: ''
              })
            });
          }

          this.editorData.stopsList.mainRoute.stops = mainRouteStops.sort((a, b) => a.sequenceID - b.sequenceID);

          const coordinates = [...mainRouteStops.map(s => {
            return s.coordinates
          })];
          if (coordinates.length) {
            editorData.routesList.mainRoute.viewMode = lineString(coordinates, {
              color: '#0080ff',
              routeName: 'mainRoute'
            });
            editorData.routesList.mainRoute.viewMode.distance = this.proposalInfo.forwardLength || length(editorData.routesList.mainRoute.viewMode) * 1000;
            editorData.routesList.mainRoute.viewMode.duration = this.proposalInfo.forwardTime || (editorData.routesList.mainRoute.viewMode.distance / 1000 / 16.6 * 60 * 60);
            editorData.routesList.mainRoute.viewMode.speed = this.proposalInfo.forwardSpeed || 16.6;
            this.$bus.$emit('fitBounds', {
              coordinates
            });
          }


        } else {
          this.addMetroStations();
          this.addJdStations();
          this.addMcdStations();

          let originalMainRoute;
          let originalBackRoute;
          const originalMainRouteStops = [];
          const originalBackRouteStops = [];
          const originalMainRouteStopsById = {};
          const originalBackRouteStopsById = {};

          if (this.proposalInfo.routeID) {
            this.routesByID.forEach(r => {
              if (r.properties.dir === 0) {
                originalMainRoute = r;
                editorData.routeProperties.mainRouteLengthKm.value = +(r.properties.length / 1000).toFixed(1);
                editorData.routeProperties.mainRouteTime.value = Math.floor((editorData.routeProperties.mainRouteLengthKm.value / 16.6) * 60);
              }
              if (r.properties.dir === 1) {
                originalBackRoute = r;
                editorData.routeProperties.backRouteLengthKm.value = +(r.properties.length / 1000).toFixed(1);
                editorData.routeProperties.backRouteTime.value = Math.floor((editorData.routeProperties.backRouteLengthKm.value / 16.6) * 60);
              }
            });
            editorData.routeProperties.lengthKm.value = ((editorData.routeProperties.mainRouteLengthKm.value || 0) + (editorData.routeProperties.backRouteLengthKm.value || 0)).toFixed(1);
            editorData.routeProperties.time.value = ((editorData.routeProperties.mainRouteTime.value || 0) + (editorData.routeProperties.backRouteTime.value || 0)).toFixed(2);

            this.stopsByRouteID.forEach(stop => {
              if (originalMainRoute && originalMainRoute.properties.stopIDs.includes(stop.properties.stopID) && stop.properties.dir === 0) {
                originalMainRouteStopsById[stop.id] = stop;
                originalMainRouteStops.push(stop);
              }
              originalMainRouteStops.sort((a, b) => a.properties.sequenceID - b.properties.sequenceID);
              if (originalBackRoute && originalBackRoute.properties.stopIDs.includes(stop.properties.stopID) && stop.properties.dir === 1) {
                originalBackRouteStopsById[stop.id] = stop;
                originalBackRouteStops.push(stop);
              }
              originalBackRouteStops.sort((a, b) => a.properties.sequenceID - b.properties.sequenceID);
            });
            editorData.routeProperties.stopsNum.value = originalMainRouteStops.length + originalBackRouteStops.length;
            editorData.routeProperties.intervalMinutes.value = originalMainRoute ? Math.round(1 / originalMainRoute.properties.vehiclesNum * editorData.routeProperties.lengthKm.value / 16.6 * 60) : null;
            editorData.routeProperties.vehiclesNum.value = originalMainRoute.properties.vehiclesNum;
            if (!this.proposalInfo.forwardStopsParams && originalMainRouteStops.length) {
              this.proposalInfo.forwardStopsParams = {
                stopUpd: originalMainRouteStops.map(s => ({
                  id: s.id,
                  stopID: s.properties.stopID,
                  coordinates: s.geometry.coordinates,
                  name: s.properties.name,
                  fullname: s.properties.fullname,
                  sequenceID: s.properties.sequenceID
                })),
                stopsNew: [],
                stopsDel: []
              };
            }
            if (!this.proposalInfo.backwardStopsParams && originalBackRouteStops.length) {
              this.proposalInfo.backwardStopsParams = {
                stopUpd: originalBackRouteStops.map(s => ({
                  id: s.id,
                  stopID: s.properties.stopID,
                  coordinates: s.geometry.coordinates,
                  name: s.properties.name,
                  fullname: s.properties.fullname,
                  sequenceID: s.properties.sequenceID
                })),
                stopsNew: [],
                stopsDel: []
              };
            }
            this.startRoutePropertiesInit();

          }

          const mainRouteStops = [];
          const backRouteStops = [];
          /*
            old - (true - удаленная из изначального маршрута, false - существующая неизмененная)
            new - новая остановка на маршруте
            type - user OR master
          */
          if (this.proposalInfo.forwardStopsParams) {
            this.proposalInfo.forwardStopsParams.stopUpd.forEach(stop => {
              const masterStop = this.stops.find(s => s.properties.stopID === stop.stopID);
              mainRouteStops.push({
                coordinates: stop.coordinates,
                enabled: true,
                id: masterStop ? masterStop.id : '',
                old: false,
                new: false,
                sequenceID: stop.sequenceID,
                text: stop.name,
                type: masterStop ? 'master' : 'user',
                value: masterStop ? masterStop.id : ''
              })
            });
            this.proposalInfo.forwardStopsParams.stopsNew.forEach((stop, index) => {
              const masterStop = this.stops.find(s => s.properties.stopID === stop.stopID);
              mainRouteStops.push({
                coordinates: stop.coordinates,
                enabled: true,
                id: masterStop ? masterStop.id : stop.id || `main-stopNew-${index}`,
                old: false,
                new: true,
                sequenceID: stop.sequenceID,
                text: stop.name,
                type: masterStop ? 'master' : 'user',
                value: masterStop ? masterStop.id : stop.id || `main-stopNew-${index}`
              })
            });
            mainRouteStops.sort((a, b) => a.sequenceID - b.sequenceID);
            if (mainRouteStops.length) {
              mainRouteStops[0].term = true;
              mainRouteStops[mainRouteStops.length - 1].term = true;
            }
            this.editorData.stopsList.mainRoute.stops = mainRouteStops.sort((a, b) => a.sequenceID - b.sequenceID);
            this.proposalInfo.forwardStopsParams.stopsDel.forEach(stop => {
              const masterStop = this.stops.find(s => s.properties.stopID === stop.stopID);
              if (masterStop) {
                mainRouteStops.splice(stop.sequenceID - 1, 0, {
                  enabled: true,
                  id: masterStop ? masterStop.id : '',
                  old: true,
                  new: false,
                  sequenceID: stop.sequenceID,
                  text: masterStop.properties.name,
                  type: 'master',
                  value: masterStop ? masterStop.id : ''
                })
              }

            });
          }

          if (this.proposalInfo.backwardStopsParams) {
            this.proposalInfo.backwardStopsParams.stopUpd.forEach(stop => {
              const masterStop = this.stops.find(s => s.properties.stopID === stop.stopID);
              backRouteStops.push({
                coordinates: stop.coordinates,
                enabled: true,
                id: masterStop ? masterStop.id : '',
                old: false,
                new: false,
                sequenceID: stop.sequenceID,
                text: stop.name,
                type: masterStop ? 'master' : 'user',
                value: masterStop ? masterStop.id : ''
              })
            });
            this.proposalInfo.backwardStopsParams.stopsNew.forEach((stop, index) => {
              const masterStop = this.stops.find(s => s.properties.stopID === stop.stopID);
              backRouteStops.push({
                coordinates: stop.coordinates,
                enabled: true,
                id: masterStop ? masterStop.id : stop.id || `back-stopNew-${index}`,
                old: false,
                new: true,
                sequenceID: stop.sequenceID,
                text: stop.name,
                type: masterStop ? 'master' : 'user',
                value: masterStop ? masterStop.id : stop.id || `back-stopNew-${index}`
              })
            });
            backRouteStops.sort((a, b) => a.sequenceID - b.sequenceID);
            if (backRouteStops.length) {
              backRouteStops[0].term = true;
              backRouteStops[backRouteStops.length - 1].term = true;
            }
            this.editorData.stopsList.backRoute.stops = backRouteStops.sort((a, b) => a.sequenceID - b.sequenceID);
            this.proposalInfo.backwardStopsParams.stopsDel.forEach(stop => {
              const masterStop = this.stops.find(s => s.properties.stopID === stop.stopID);
              if (masterStop) {
                backRouteStops.splice(stop.sequenceID - 1, 0, {
                  enabled: true,
                  id: masterStop ? masterStop.id : '',
                  old: true,
                  new: false,
                  sequenceID: stop.sequenceID,
                  text: masterStop.properties.name,
                  type: 'master',
                  value: masterStop ? masterStop.id : ''
                })
              }
            });
          }

          // generate routes Data
          let coordinates = [];

          if (this.proposalInfo.fgeom) {
            coordinates = coordinates.concat(this.proposalInfo.fgeom);
            editorData.routesList.mainRoute.viewMode = multiLineString(this.proposalInfo.fgeom, {
              color: '#0080ff',
              routeName: 'mainRoute'
            });
            if (this.proposalInfo.forwardLength) {
              editorData.routesList.mainRoute.viewMode.distance = this.proposalInfo.forwardLength;
              editorData.routesList.mainRoute.viewMode.duration = this.proposalInfo.forwardTime || (this.proposalInfo.forwardLength / 1000 / 16.6 * 60 * 60);
            }
            if (this.proposalInfo.forwardSpeed) {
              editorData.routesList.mainRoute.viewMode.speed = this.proposalInfo.forwardSpeed;
            }
          }

          if (this.proposalInfo.bgeom) {
            coordinates = coordinates.concat(this.proposalInfo.bgeom);
            editorData.routesList.backRoute.viewMode = multiLineString(this.proposalInfo.bgeom, {
              color: '#008000',
              routeName: 'backRoute'
            });
            if (this.proposalInfo.backwardLength) {
              editorData.routesList.backRoute.viewMode.distance = this.proposalInfo.backwardLength;
              editorData.routesList.backRoute.viewMode.duration = this.proposalInfo.backwardTime || (this.proposalInfo.backwardLength / 1000 / 16.6 * 60 * 60);
            }
            if (this.proposalInfo.backwardSpeed) {
              editorData.routesList.backRoute.viewMode.speed = this.proposalInfo.backwardSpeed;
            }
          }
          coordinates = coordinates.reduce((acc, curr) => acc.concat(curr), []);
          if (!coordinates.length) {
            coordinates = this.stopsByRouteID.map ?
              this.stopsByRouteID.map(stop => stop.geometry.coordinates) : [];
            if (!coordinates.length && this.proposalInfo.routeMainType !== 2 && this.proposalInfo.forwardStopsParams) {
              coordinates = this.proposalInfo.forwardStopsParams.stopUpd
                .concat(this.proposalInfo.forwardStopsParams.stopsNew)
                .concat(this.proposalInfo.backwardStopsParams.stopUpd)
                .concat(this.proposalInfo.backwardStopsParams.stopsNew)
                .reduce((acc, curr) => acc.concat([curr.coordinates]), [])
            }
          }
          if (coordinates.length) {
            this.$bus.$emit('fitBounds', {
              coordinates
            });
          }
        }

        this.addEditorStops();

        this.addAllStops();

        [
          'layer-editor-stops-main-route',
          'layer-editor-stops-back-route',
          'layer-editor-stops-name',
          'layer-editor-stops-cluster',
          'layer-editor-edge-stops-main-route',
          'layer-editor-edge-stops-back-route',
          'layer-editor-green-stops-main-route',
          'layer-editor-green-stops-back-route',
          'layer-editor-allStops',
          'layer-editor-allStops-names',
          'layer-editor-allStops-cluster'
        ].forEach(layer => {
          this.map.on('mouseenter', layer, () => {
            editorData.stopsHover = true;
            this.map.getCanvas().style.cursor = 'pointer'
          });
          this.map.on('mouseleave', layer, () => {
            editorData.stopsHover = false;
            this.map.getCanvas().style.cursor = 'default'
          });
          this.map.on('click', layer, (e) => {
            clearTimeout(this.stopsClickTimeOut);
            if (!this.stopsClickItems) {
              this.stopsClickItems = [];
            }
            this.stopsClickItems.push(e.features[0]);
            this.stopsClickTimeOut = setTimeout(() => {
              let items = this.stopsClickItems.filter(s => !s.properties.routeGUID);
              if (!items.length) {
                items = this.stopsClickItems;
              }
              const item = items[1] || items[0];
              this.stopsClickItems = [];
              item.layer.allStopHover = true;
              this.$bus.$emit('showMapPopup', {
                layer: 'editor',
                item,
                containerClassNames: ['editor-popup'],
              });
            });
          });
          this.map.on('mouseenter', layer, () => {
            this.map.getCanvas().style.cursor = 'pointer';
          });
          this.map.on('mouseleave', layer, () => {
            this.map.getCanvas().style.cursor = 'default';
          });
        });
      } catch (e) {
        console.error('editorInitViewMode error', e, this.proposal)
      }

      this.map.on('mousedown', 'layer-editor-lines', this.editorLinesOnMouseDown);
    }
  }
};
