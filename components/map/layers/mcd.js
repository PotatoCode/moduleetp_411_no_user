import mcd1Icon from '~/assets/img/mcd1.svg';
import mcd2Icon from '~/assets/img/mcd2.svg';
import mcd3Icon from '~/assets/img/mcd3.svg';
import mcd4Icon from '~/assets/img/mcd4.svg';
import mcd5Icon from '~/assets/img/mcd5.svg';

export const mcd = {
  methods: {
    addMcdStations() {
      this.map.addLayer({
        id: 'mcd',
        type: 'symbol',
        minzoom: 13.5,
        source: {
          type: 'geojson',
          data: {
            features: this.mcdStations,
            type: 'FeatureCollection',
          }
        },
        layout: {
          'icon-image': ['get', 'icon'],
          'icon-allow-overlap': true,
          'icon-ignore-placement': true,
          'icon-size': .25,
          'icon-offset': [150, 0],
          'text-size': {
            stops: [
              [0, 0],
              [14.49999, 0],
              [14.5, 12]
            ]
          }
        },
        paint: {
          'text-halo-color': '#fff',
          'text-halo-width': 2
        }
      });
      if (this.map.getLayer('z-index-100')) {
        this.map.moveLayer('mcd', 'z-index-100');
      }
    },
    loadMcdIcons() {
      const icons = [
        {
          name: 'icon-mcd1',
          img: mcd1Icon,
        },
        {
          name: 'icon-mcd2',
          img: mcd2Icon,
        },
        {
          name: 'icon-mcd3',
          img: mcd3Icon,
        },
        {
          name: 'icon-mcd4',
          img: mcd4Icon,
        },
        {
          name: 'icon-mcd5',
          img: mcd5Icon,
        }
      ];
      icons.forEach(icon => {
        const img = new Image(159, 80);
        img.onload = () => {
          if (this.map) {
            this.map.addImage(icon.name, img);
          }
        };
        img.src = icon.img;
      })
    }
  },
  computed: {
    mcdStations() {
      return this.$store.state.jd.stops.filter(s => s.properties.mcdNameLatin).map(s => ({
        ...s,
        properties: {
          ...s.properties,
          icon: `icon-${s.properties.mcdNameLatin}`
        }
      }));
    }
  }
};
