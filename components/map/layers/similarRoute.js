export const similarRoute = {
  methods: {
    addSimilarRoute(route) {
      const source = {
        type: 'geojson',
        data: {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'MultiLineString',
            coordinates: route.geom
          }
        }
      };
      if (!this.map.getSource(`similar-route-${route.routeID}`)) {
        this.map.addLayer({
          id: `similar-route-${route.routeID}`,
          type: 'line',
          source,
          paint: {
            'line-color': route.color,
            'line-width': 3,
            // 'line-dasharray': [1, 5]
          },
          layout: {
            'line-join': 'round',
            'line-cap': 'round'
          },
        }, 'z-index-300');
      }
      if (!this.map.getSource(`similar-route-name-${route.routeID}`)) {
        this.map.addLayer({
          id: `similar-route-name-${route.routeID}`,
          type: 'symbol',
          source,
          paint: {
            'text-color': route.color,
          },
          layout: {
            'symbol-placement': 'line',
            'text-field': route.routeName,
            'text-size': 12,
            'text-offset': [0, 1],
            'text-max-angle': 0,
          },
        }, 'z-index-300');
      }
    },
    delSimilarRoute(id) {
      if (this.map.getSource(`similar-route-${id}`)) {
        this.map.removeLayer(`similar-route-${id}`);
        this.map.removeSource(`similar-route-${id}`);
      }
      if (this.map.getSource(`similar-route-name-${id}`)) {
        this.map.removeLayer(`similar-route-name-${id}`);
        this.map.removeSource(`similar-route-name-${id}`);
      }
    }
  }
};
