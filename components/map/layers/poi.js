export const poi = {
  methods: {
    addPOI() {
      if (!this.map.getLayer('poi')) {
        this.map.addLayer({
          "id": "poi",
          "type": "symbol",
          "source": {
            "type": "vector",
            "tiles": ["https://niikeeper.com/mvp_test/v0.4.2/moscow/poi/mvt/{z}/{x}/{y}"],
            "minzoom": 16,
          },
          "layout": {
            "icon-image": ["concat", ["get", "category"], "-color-15"]
           },
          "source-layer": "poi_dit",
        });
      }
    }
  }
};
